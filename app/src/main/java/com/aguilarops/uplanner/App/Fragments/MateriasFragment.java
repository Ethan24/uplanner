package com.aguilarops.uplanner.App.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aguilarops.uplanner.App.Activities.Materias.AddMateriaActivity;
import com.aguilarops.uplanner.App.RecyclerAdapters.MateriasFragmentAdapter;
import com.aguilarops.uplanner.R;
import com.aguilarops.uplanner.bd.bdMateria;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MateriasFragment extends Fragment {

    public MateriasFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_materias, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerview_materias);
        MateriasFragmentAdapter adapter = new MateriasFragmentAdapter(bdMateria.getMaterias(getContext()));
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        FloatingActionButton floatingActionButton = view.findViewById(R.id.fab_materias);
        floatingActionButton.setOnClickListener(v -> v.getContext().startActivity(
                new Intent(v.getContext(), AddMateriaActivity.class)));

        return view;
    }
}