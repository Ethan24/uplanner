package com.aguilarops.uplanner.App;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.aguilarops.uplanner.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class ShortDialog {
    Activity activity;
    View mview;
    AlertDialog dialog;
    TextView txt_title;
    TextView txt_message;
    MaterialButton button;

    @SuppressLint({"InflateParams", "SetTextI18n"})
    public ShortDialog(Activity activity, String title, String message) {
        this.activity = activity;
        mview = activity.getLayoutInflater().inflate(R.layout.dialog_shortinfo, null);
        dialog = new MaterialAlertDialogBuilder(activity)
                .setView(mview).create();
        txt_title = mview.findViewById(R.id.textView_title);
        txt_message = mview.findViewById(R.id.textView_message);
        txt_title.setText(title);
        txt_message.setText(message);
        button = mview.findViewById(R.id.button);
        button.setText("Ok");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void Show() {
        dialog.show();
    }
}
