package com.aguilarops.uplanner.App.RecyclerAdapters;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aguilarops.uplanner.App.Activities.Materias.DetalleMateriaActivity;
import com.aguilarops.uplanner.R;
import com.aguilarops.uplanner.abstraction.appdata.materias.Materia;
import com.google.android.material.card.MaterialCardView;

import java.util.ArrayList;

public class MateriasFragmentAdapter extends RecyclerView.Adapter<MateriasFragmentAdapter.MateriaCardViewHolder> {

    private ArrayList<Materia> materias;

    public MateriasFragmentAdapter(ArrayList<Materia> materias) {
        this.materias = materias;
    }

    @NonNull
    @Override
    public MateriaCardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MateriaCardViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_materias, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MateriaCardViewHolder holder, int position) {
        Materia materia = materias.get(position);
        holder.name.setText(materia.name);
        holder.color.setImageTintList(ColorStateList.valueOf(materia.color));
        holder.grupo.setText(materia.grupo);
        holder.proxima_sesion.setText(materia.nextClass);

        holder.cardView.setOnClickListener(V -> {
            Intent intent = new Intent(V.getContext(), DetalleMateriaActivity.class);
            intent.putExtra("Nombre", materia.name);
            intent.putExtra("Grupo", materia.grupo);
            intent.putExtra("Color", materia.color);
            intent.putExtra("Dias", materia.dias);
            V.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return materias.size();
    }

    static class MateriaCardViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView color;
        TextView grupo;
        TextView proxima_sesion;
        MaterialCardView cardView;

        public MateriaCardViewHolder(@NonNull View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.cardview_materias);
            name = itemView.findViewById(R.id.materias_tx_nombre);
            color = itemView.findViewById(R.id.materias_circle_color);
            grupo = itemView.findViewById(R.id.materias_tx_grupo);
            proxima_sesion = itemView.findViewById(R.id.materias_tx_prox_sesion);
        }
    }
}
