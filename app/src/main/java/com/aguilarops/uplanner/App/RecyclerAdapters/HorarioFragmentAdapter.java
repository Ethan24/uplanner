package com.aguilarops.uplanner.App.RecyclerAdapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aguilarops.uplanner.R;
import com.aguilarops.uplanner.abstraction.appdata.materias.Bloque;

import java.util.ArrayList;

public class HorarioFragmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Bloque> bloques;

    public HorarioFragmentAdapter(ArrayList<Bloque> bloques) {
        this.bloques = bloques;
    }


    @Override
    public int getItemCount() {
        return bloques.size();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.horario_item, parent, false);

        return new HorarioItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
//            System.out.println(position);
            HorarioItemViewHolder itemViewHolder = (HorarioItemViewHolder) holder;
//            System.out.println(bloques.get(position).materia.color);
            itemViewHolder.background.setBackgroundColor(bloques.get(position).materia.color);
            itemViewHolder.nombre.setText(bloques.get(position).materia.name);
            itemViewHolder.seccion.setText(bloques.get(position).seccion);
    }

    static class HorarioItemViewHolder extends RecyclerView.ViewHolder {
        LinearLayout background;
        TextView nombre, seccion;

        public HorarioItemViewHolder(@NonNull View itemView) {
            super(itemView);
            this.background = itemView.findViewById(R.id.Horario_item_background);
            this.nombre = itemView.findViewById(R.id.Horario_item_clase);
            this.seccion = itemView.findViewById(R.id.Horario_item_seccion);
        }
    }
}
