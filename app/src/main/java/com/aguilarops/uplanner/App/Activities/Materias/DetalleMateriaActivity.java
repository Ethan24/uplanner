package com.aguilarops.uplanner.App.Activities.Materias;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.aguilarops.uplanner.R;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.chip.Chip;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import java.util.ArrayList;
import java.util.Objects;


public class DetalleMateriaActivity extends AppCompatActivity {

    private TextView nombre_tx, grupo_tx, seccion_tx, edificio_tx, docente_tx;
    private Chip[] days_chips;
    private int color;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_materia);

        color = getIntent().getIntExtra("Color", getResources().getColor(R.color.colorAccent, null));
        createDaysChips(color);

        MaterialToolbar toolbar = findViewById(R.id.toolbar_detalles_materia);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        nombre_tx = findViewById(R.id.detalles_materia_nombre);
        nombre_tx.setText(getIntent().getStringExtra("Nombre"));

        grupo_tx = findViewById(R.id.detalles_materia_grupo);
        grupo_tx.setText(getIntent().getStringExtra("Grupo"));
        seccion_tx = findViewById(R.id.detalles_materia_seccion);
        edificio_tx = findViewById(R.id.detalles_materia_edificio);
        docente_tx = findViewById(R.id.detalles_materia_docente);

        ExtendedFloatingActionButton floatingActionButton = findViewById(R.id.fb_detalles_materia);
        floatingActionButton.setBackgroundTintList(ColorStateList.valueOf(color));
        floatingActionButton.setOnClickListener(v -> {
            Intent intent = new Intent(DetalleMateriaActivity.this, EditMateriaActivity.class);
            intent.putExtra("Nombre", getIntent().getStringExtra("Nombre"));
            intent.putExtra("Grupo", getIntent().getStringExtra("Grupo"));
            intent.putExtra("Color", getIntent().getIntExtra("Color",
                    getResources().getColor(R.color.colorAccent, null)));
            v.getContext().startActivity(intent);
        });
    }

    private void createDaysChips(int color) {
        days_chips = new Chip[]{
                findViewById(R.id.chip_dom_dm), findViewById(R.id.chip_lun_dm),
                findViewById(R.id.chip_mar_dm), findViewById(R.id.chip_mie_dm),
                findViewById(R.id.chip_jue_dm), findViewById(R.id.chip_vie_dm),
                findViewById(R.id.chip_sab_dm)
        };

        checkDaysChips(Objects.requireNonNull(getIntent().getIntegerArrayListExtra("Dias")), color);
    }

    private void checkDaysChips(ArrayList<Integer> dias, int color) {

        System.out.println("dias.size():" + dias.size());
        if (dias.size() > 0) {
            ColorStateList chip_color = ColorStateList.valueOf(color);
            for (int i : dias) {
                System.out.println(i);
                days_chips[i].setChipBackgroundColor(chip_color);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishActivity(0);
    }
}