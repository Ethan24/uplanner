package com.aguilarops.uplanner.App.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.aguilarops.uplanner.App.Fragments.BooksFragment;
import com.aguilarops.uplanner.App.Fragments.DocentesFragment;
import com.aguilarops.uplanner.App.Fragments.GalleryFragment;
import com.aguilarops.uplanner.App.Fragments.HomeFragment;
import com.aguilarops.uplanner.App.Fragments.HorarioFragment;
import com.aguilarops.uplanner.App.Fragments.MateriasFragment;
import com.aguilarops.uplanner.App.Fragments.SettingsFragment;
import com.aguilarops.uplanner.App.Fragments.TareasFragment;
import com.aguilarops.uplanner.R;
import com.aguilarops.uplanner.abstraction.user.horario.Horario;
import com.aguilarops.uplanner.bd.bdBloque;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;

public class HomeActivity extends AppCompatActivity {

    private Fragment mFragmentToSet;
    private String previousFragmentTag, mFragmentToSetTag, title;
    private DrawerLayout drawerLayout;

    private MaterialToolbar toolbar;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setupDrawerLayoutWithToolbar();
        configDrawerLayout();
        createHomeFragment();
        setNavigationViewOnItemSelectedAction();
        setFragment("Home");
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
        } else if (previousFragmentTag != null) {
            setFragment(previousFragmentTag);
            toolbar.setTitle("Inicio");
            navigationView.setCheckedItem(R.id.menu_item_home);
            previousFragmentTag = null;
            mFragmentToSet = null;
        } else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }


    private void createHomeFragment() {
        createFragmentToSet("Home");
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, mFragmentToSet);
    }

    private void setNavigationViewOnItemSelectedAction() {
        navigationView = findViewById(R.id.navigation);
        navigationView.setNavigationItemSelectedListener(item -> {
            if (item.getItemId() != R.id.menu_item_home)
                previousFragmentTag = "Home";

            switch (item.getItemId()) {
                case R.id.menu_item_home:
                    title = "Inicio";
                    mFragmentToSetTag = "Home";
                    createFragmentToSet(mFragmentToSetTag);
                    break;

                case R.id.menu_item_events:
                    title = "Eventos";
                    mFragmentToSetTag = "Eventos";
                    createFragmentToSet(mFragmentToSetTag);
                    break;

                case R.id.menu_item_materias:
                    title = "Materias";
                    mFragmentToSetTag = "Materias";
                    createFragmentToSet(mFragmentToSetTag);
                    break;

                case R.id.menu_item_horario:
                    title = "Horario";
                    mFragmentToSetTag = "Horario";
                    createFragmentToSet(mFragmentToSetTag);
                    break;

                case R.id.menu_item_books:
                    title = "Libros";
                    mFragmentToSetTag = "Libros";
                    createFragmentToSet(mFragmentToSetTag);
                    break;

                case R.id.menu_item_galeria:
                    title = "Galeria";
                    mFragmentToSetTag = "Galeria";
                    createFragmentToSet(mFragmentToSetTag);
                    break;

                case R.id.menu_item_profesores:
                    title = "Docentes";
                    mFragmentToSetTag = "Docentes";
                    createFragmentToSet(mFragmentToSetTag);
                    break;

                case R.id.menu_item_apoyo:
                    break;

                case R.id.menu_item_settings:
                    title = "Configuracion";
                    mFragmentToSetTag = "Settings";
                    createFragmentToSet(mFragmentToSetTag);
                    break;
            }
            drawerLayout.closeDrawers();
            return true;
        });
    }


    private void setupDrawerLayoutWithToolbar() {
        toolbar = findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawerLayout);
        toolbar.setNavigationOnClickListener(v -> {
            if (drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.closeDrawer(GravityCompat.END);
            else
                drawerLayout.openDrawer(GravityCompat.START);
        });
    }

    private void configDrawerLayout() {
        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                if (mFragmentToSet != null && mFragmentToSetTag != null) {
                    setFragment(mFragmentToSetTag);
                    mFragmentToSet = null;
                    mFragmentToSetTag = null;
                    toolbar.setTitle(title);
                    title = null;
                }
            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }
        });
    }

    private void setFragment(String Tag) {
        createFragmentToSet(Tag);
        getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(R.id.nav_host_fragment, mFragmentToSet)
                .commit();
    }

    private void createFragmentToSet(String Tag) {
        switch (Tag) {
            case "Home":
                mFragmentToSet = new HomeFragment(bdBloque.getbloques_perDay2home(getApplicationContext(),
                        Horario.getDay()));
                break;

            case "Eventos":
                mFragmentToSet = new TareasFragment();
                break;

            case "Horario":
                mFragmentToSet = new HorarioFragment();
                break;

            case "Materias":
                mFragmentToSet = new MateriasFragment();
                break;

            case "Libros":
                mFragmentToSet = new BooksFragment();
                break;

            case "Galeria":
                mFragmentToSet = new GalleryFragment();
                break;

            case "Docentes":
                mFragmentToSet = new DocentesFragment();
                break;

            case "Settings":
                mFragmentToSet = new SettingsFragment();
                break;
        }
    }

    public void toHorarioFragment(View view) {
        previousFragmentTag = "Home";
        mFragmentToSetTag = "Horario";
        setFragment(mFragmentToSetTag);
        navigationView.setCheckedItem(R.id.menu_item_horario);
        toolbar.setTitle("Horario");
    }


    public void toEventsFragment(View view) {
        previousFragmentTag = "Home";
        mFragmentToSetTag = "Eventos";
        setFragment(mFragmentToSetTag);
        navigationView.setCheckedItem(R.id.menu_item_events);
        toolbar.setTitle("Eventos");
    }
}