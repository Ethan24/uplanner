package com.aguilarops.uplanner.App.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.aguilarops.uplanner.App.Activities.FillData.LoadUserDataActivity;
import com.aguilarops.uplanner.R;

import static com.aguilarops.uplanner.bd.bdUtil.isThereLogged;


public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final Context c = getApplicationContext();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent;
                if (isThereLogged(c)) {
                    intent = new Intent(SplashActivity.this, HomeActivity.class);
                } else {
                    intent = new Intent(SplashActivity.this, LoadUserDataActivity.class);
                }
                startActivity(intent);
            }
        }, 400);
    }
}