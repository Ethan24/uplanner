package com.aguilarops.uplanner.App.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.aguilarops.uplanner.R;
import com.aguilarops.uplanner.abstraction.appdata.materias.Bloque;
import com.aguilarops.uplanner.bd.bdBloque;
import com.aguilarops.uplanner.util.Timetable.Schedule;
import com.aguilarops.uplanner.util.Timetable.Time;
import com.aguilarops.uplanner.util.Timetable.TimetableView;

import java.util.ArrayList;
import java.util.List;

public class HorarioFragment extends Fragment {

    public HorarioFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_horario, container, false);

        TimetableView timetableView = view.findViewById(R.id.timetable);

        ArrayList<Schedule> schedules = new ArrayList<>();
        List<Bloque> bloques = bdBloque.getHorario(view.getContext());
        for (Bloque i : bloques) {
            Schedule schedule = new Schedule();
            schedule.setColor(i.materia.color);
            schedule.setClassTitle(i.materia.name);
            schedule.setDay(i.day - 1);
            schedule.setClassPlace(i.seccion);
            schedule.setStartTime(new Time(i.inicio.horas,i.inicio.minutos));
            schedule.setEndTime(new Time(i.fin.horas,i.fin.minutos));

            schedules.add(schedule);
        }
        timetableView.add(schedules);
        return view;
    }

}