package com.aguilarops.uplanner.App.Activities.FillData;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.aguilarops.uplanner.App.Activities.HomeActivity;
import com.aguilarops.uplanner.App.Fragments.DataLoadedFragment;
import com.aguilarops.uplanner.R;
import com.aguilarops.uplanner.abstraction.appdata.materias.Bloque;
import com.aguilarops.uplanner.bd.bdMateria;
import com.aguilarops.uplanner.bd.bdProfesor;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.rw.keyboardlistener.KeyboardUtils;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import java.util.ArrayList;
import java.util.List;

public class DataLoadedActivity extends AppCompatActivity {

    private ArrayList<Bloque> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_loaded);

        int[] colors = (getResources().getIntArray(R.array.color_pallete));

        ViewPager viewPager = findViewById(R.id.viewPagerAsignaturas);
        data = bdMateria.getAsignaturasLoaded(getApplicationContext());
        AdaptadorViewPager adaptadorViewPager = new AdaptadorViewPager(getSupportFragmentManager());

        for (int i = 0; i < data.size(); i++) {
            data.get(i).materia.profesor.correo = null;
            data.get(i).materia.profesor.telefono = null;
            bdMateria.materia_set_color(getApplicationContext(), data.get(i).materia.id, colors[i]);
            adaptadorViewPager.add(new DataLoadedFragment(colors[i], i, data.get(i)));
        }

        viewPager.setAdapter(adaptadorViewPager);

        final DotsIndicator indicators = findViewById(R.id.dots_indicator_Data);
        indicators.setViewPager(viewPager);

        final FloatingActionButton btn_done = findViewById(R.id.done_btn);
        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (Bloque b : data) {
                    String telef = b.materia.profesor.telefono;
                    String email = b.materia.profesor.correo;
                    if (telef != null || email != null) {
                        bdProfesor.setProfesor_contact(getApplicationContext(), telef, email, b.materia.profesor.id);
                    }
                }
                startActivity(new Intent(DataLoadedActivity.this, HomeActivity.class));
            }
        });

        KeyboardUtils.addKeyboardToggleListener(this, new KeyboardUtils.SoftKeyboardToggleListener() {
            @Override
            public void onToggleSoftKeyboard(final boolean isVisible) {
                if (isVisible) {
                    btn_done.setVisibility(View.GONE);
                    indicators.setVisibility(View.GONE);
                } else {
                    indicators.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            btn_done.setVisibility(View.VISIBLE);
                        }
                    }, 20);
                }


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, 50);
            }
        });

    }

    public void addEmail(String email, int index) {
        data.get(index).materia.profesor.correo = email;
    }

    public void addNumber(String num, int index) {
        data.get(index).materia.profesor.telefono = num;
    }

    static class AdaptadorViewPager extends FragmentPagerAdapter {
        private final List<Fragment> listaDeFragmentos = new ArrayList<>();


        AdaptadorViewPager(FragmentManager manager) {
            super(manager, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }


        @NonNull
        @Override
        public Fragment getItem(int position) {
            return listaDeFragmentos.get(position);
        }

        @Override
        public int getCount() {
            return listaDeFragmentos.size();
        }


        void add(Fragment fragment) {
            listaDeFragmentos.add(fragment);
        }
    }
}