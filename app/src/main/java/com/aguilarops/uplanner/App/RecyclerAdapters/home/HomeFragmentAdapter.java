package com.aguilarops.uplanner.App.RecyclerAdapters.home;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aguilarops.uplanner.App.model.XAxisValueFormat;
import com.aguilarops.uplanner.R;
import com.aguilarops.uplanner.abstraction.appdata.materias.Bloque;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.chip.Chip;

import java.util.ArrayList;

public class HomeFragmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Bloque> bloques;
    private LineData data;

    public HomeFragmentAdapter(ArrayList<Bloque> bloques, LineData data) {
        this.bloques = bloques;
        this.data = data;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case 1:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_horario_diario, parent, false);
                break;
            case 2:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_reporte, parent, false);
                break;
            case 3:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.date_divider_layout, parent, false);
                break;
            case 4:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_eventos_item, parent, false);
                break;
        }
        return (viewType == 1) ? new HorarioViewHolder(view) : new CardReporteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        switch (getItemViewType(position)) {
            case 1:
                HorarioViewHolder viewHolder = (HorarioViewHolder) holder;
                viewHolder.list.setAdapter(new HorarioDiarioAdapter(bloques));
                LinearLayoutManager layoutManager = new LinearLayoutManager(holder.itemView.getContext());
                layoutManager.setOrientation(RecyclerView.VERTICAL);
                viewHolder.list.setLayoutManager(layoutManager);
                break;

            case 2:
                CardReporteViewHolder cardReporteViewHolder = (CardReporteViewHolder) holder;
                cardReporteViewHolder.chart_report.setData(data);
                cardReporteViewHolder.chart_report.getLineData().setDrawValues(false);
                cardReporteViewHolder.chart_report.getXAxis().setGridColor(R.color.lightgray);
                cardReporteViewHolder.chart_report.getXAxis().setDrawAxisLine(false);
                cardReporteViewHolder.chart_report.getXAxis().setValueFormatter(new XAxisValueFormat());
                cardReporteViewHolder.chart_report.getLegend().setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
                cardReporteViewHolder.chart_report.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
                cardReporteViewHolder.chart_report.getAxisLeft().setDrawGridLines(false);
                cardReporteViewHolder.chart_report.getAxisLeft().setDrawLabels(false);
                cardReporteViewHolder.chart_report.getAxisLeft().setDrawAxisLine(false);
                cardReporteViewHolder.chart_report.getAxisRight().setDrawGridLines(false);
                cardReporteViewHolder.chart_report.getAxisRight().setDrawAxisLine(false);
                cardReporteViewHolder.chart_report.getAxisRight().setDrawLabels(false);
                cardReporteViewHolder.chart_report.getDescription().setEnabled(false);
                cardReporteViewHolder.chart_report.getLegend().setEnabled(false);
                cardReporteViewHolder.chart_report.setTouchEnabled(false);
                break;

            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        //reemplazar por eventos.size()+2;
        return 10;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return 1;
        else if (position == 1)
            return 2;
        else if (position == 2 || position == 6) //cambiar por objetoEvento.fecha es diferente a objetoEvento.fecha siguiente
            return 3;
        else
            return 4;
    }

    static class HorarioViewHolder extends RecyclerView.ViewHolder {

        RecyclerView list;

        public HorarioViewHolder(@NonNull View itemView) {
            super(itemView);
            list = itemView.findViewById(R.id.horario_cardslider);
        }
    }

    static class CardReporteViewHolder extends RecyclerView.ViewHolder {
        LineChart chart_report;
        MaterialButton btn_ver_mas;

        public CardReporteViewHolder(@NonNull View itemView) {
            super(itemView);
            this.chart_report = itemView.findViewById(R.id.semanal_report_chart);
            this.btn_ver_mas = itemView.findViewById(R.id.btn_ver_mas_card_reporte);
        }
    }

    static class DateItemViewHolder extends RecyclerView.ViewHolder {
        TextView tx_date;
        public DateItemViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tx_date = itemView.findViewById(R.id.txt_date_separator);
        }
    }

    static class CardEventoViewHolder extends RecyclerView.ViewHolder {
        Chip tag;
        TextView descripcion_tx, materia_tx, fecha_tx, hora_tx;

        public CardEventoViewHolder(@NonNull View itemView) {
            super(itemView);
            this.descripcion_tx = itemView.findViewById(R.id.home_event_item_descripcion);
            this.materia_tx = itemView.findViewById(R.id.home_event_item_materia_name);
            this.fecha_tx = itemView.findViewById(R.id.home_event_item_dia_entrega);
            this.hora_tx = itemView.findViewById(R.id.home_event_item_hora_entrega);
            this.tag = itemView.findViewById(R.id.home_event_tag_item);
        }
    }

}
