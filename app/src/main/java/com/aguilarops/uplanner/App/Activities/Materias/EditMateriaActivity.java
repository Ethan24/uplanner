package com.aguilarops.uplanner.App.Activities.Materias;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aguilarops.uplanner.App.Activities.Docentes.EditDocenteActivity;
import com.aguilarops.uplanner.App.RecyclerAdapters.SelectDocenteAdapter;
import com.aguilarops.uplanner.R;
import com.aguilarops.uplanner.abstraction.appdata.materias.Materia;
import com.aguilarops.uplanner.bd.bdProfesor;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.thebluealliance.spectrum.SpectrumDialog;

import java.util.ArrayList;

public class EditMateriaActivity extends AppCompatActivity {

    private int color;
    AlertDialog dialog;
    private TextView docente_tx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_materia);

        EditText materia_tx, grupo_tx, edificio_tx, seccion_tx;

        materia_tx = findViewById(R.id.edit_materia_tx_nombre);
        materia_tx.setText(getIntent().getStringExtra("Nombre"));

        grupo_tx = findViewById(R.id.edit_materia_tx_grupo);
        grupo_tx.setText(getIntent().getStringExtra("Grupo"));

        edificio_tx = findViewById(R.id.edit_materia_tx_edificio);

        seccion_tx = findViewById(R.id.edit_materia_tx_salon);
        docente_tx = findViewById(R.id.editar_docente_materia);
        docente_tx.setOnClickListener(this::selectDocente);

        color = getIntent().getIntExtra("Color", getResources().getColor(R.color.colorAccent, null));

        MaterialButton color_picker_btn = findViewById(R.id.color_picker_btn_edit_mat);
        color_picker_btn.setIconTint(ColorStateList.valueOf(color));
        color_picker_btn.setOnClickListener(l -> {
            SpectrumDialog dialog = new SpectrumDialog.Builder(this)
                    .setColors(getResources()
                            .getIntArray(R.array.color_pallete))
                    .setSelectedColor(color)
                    .build();

            dialog.show(getSupportFragmentManager(), "ColorPicker");
            dialog.setOnColorSelectedListener((positiveResult, color) -> {

            });
        });

        ExtendedFloatingActionButton floatingActionButton = findViewById(R.id.fb_edit_materia);
        floatingActionButton.setOnClickListener(v -> onBackPressed());

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishActivity(0);
    }

    public void selectDocente(View v) {
        ArrayList<Materia> docentes = bdProfesor.getList_of_Docentes2Select(getApplicationContext());

        @SuppressLint("InflateParams")
        View view1 = LayoutInflater.from(this).inflate(R.layout.simple_recycler, null);
        RecyclerView recyclerView = view1.findViewById(R.id.reciclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(view1.getContext()));
        SelectDocenteAdapter adapter = new SelectDocenteAdapter(docentes, EditMateriaActivity.this);
        recyclerView.setAdapter(adapter);


        dialog = new MaterialAlertDialogBuilder(this, R.style.AlertDialog)
                .setTitle("Docente")
                .setView(view1)
                .setCancelable(true)
                .setPositiveButton("Crear Nuevo", (dialog, which) -> {
                    Intent intent = new Intent(EditMateriaActivity.this, EditDocenteActivity.class);
                    intent.putExtra(Activity.ACTIVITY_SERVICE, EditMateriaActivity.class.getSimpleName());
                    startActivity(intent);
                }).show();
    }

    public void docente_change(Materia result) {
        if (result != null) {
            docente_tx.setText(result.profesor.name);
            dialog.dismiss();
        }
    }
}