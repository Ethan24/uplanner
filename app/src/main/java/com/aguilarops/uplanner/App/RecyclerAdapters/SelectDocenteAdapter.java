package com.aguilarops.uplanner.App.RecyclerAdapters;

import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aguilarops.uplanner.App.Activities.Materias.EditMateriaActivity;
import com.aguilarops.uplanner.R;
import com.aguilarops.uplanner.abstraction.appdata.materias.Materia;
import com.aguilarops.uplanner.abstraction.appdata.materias.Profesor;

import java.util.ArrayList;

public class SelectDocenteAdapter extends RecyclerView.Adapter<SelectDocenteAdapter.ItemViewHolder> {

    ArrayList<Materia> profesors;
    EditMateriaActivity parent;

    public SelectDocenteAdapter(ArrayList<Materia> profesors, EditMateriaActivity parent) {
        this.profesors = profesors;
        this.parent = parent;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_docente_select, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, final int position) {
        Profesor profesor = profesors.get(position).profesor;
        int color = profesors.get(position).color;
        String name = profesor.name;
        String[] names = name.split(" ");
        String letras = names[0].substring(0, 1).concat(names[1].substring(0, 1));
        holder.nombre.setText(profesor.name);
        holder.iniciales.setText(letras);
        holder.iniciales_bk.setImageTintList(ColorStateList.valueOf(color));
        holder.item.setOnClickListener(v -> parent.docente_change(profesors.get(position)));
    }

    @Override
    public int getItemCount() {
        return profesors.size();
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView nombre;
        TextView iniciales;
        LinearLayout item;
        ImageView iniciales_bk;

        ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            this.nombre = itemView.findViewById(R.id.nombre_profesor_select);
            item = itemView.findViewById(R.id.item_select_docente);
            iniciales_bk = itemView.findViewById(R.id.docente_circle_background_iniciales_select);
            iniciales = itemView.findViewById(R.id.circle_inicial_profesor_select);
        }
    }

}
