package com.aguilarops.uplanner.App.RecyclerAdapters;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aguilarops.uplanner.App.Activities.Docentes.DetallesDocenteActivity;
import com.aguilarops.uplanner.R;
import com.aguilarops.uplanner.abstraction.appdata.materias.Materia;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;

import java.util.ArrayList;

public class DocentesFragmentAdapter extends RecyclerView.Adapter<DocentesFragmentAdapter.DocenteViewHolder> {
    private ArrayList<Materia> materias;

    public DocentesFragmentAdapter(ArrayList<Materia> materias) {
        this.materias = materias;
    }

    @NonNull
    @Override
    public DocenteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DocenteViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_profesor, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull DocenteViewHolder holder, int position) {
        Materia materia = materias.get(position);

        final String name = materia.profesor.name;
        String[] names = name.split(" ");
        final String letras = names[0].substring(0, 1).concat(names[1].substring(0, 1));

        holder.profesor_nombre.setText(materia.profesor.name);
        holder.profesor_iniciales.setText(letras);
        holder.materia_nombre.setText(materia.name);
        holder.iniciales_bk.setImageTintList(ColorStateList.valueOf(materia.color));

        if (materia.profesor.correo == null && materia.profesor.telefono == null)
            holder.divider2.setVisibility(View.GONE);

        if (materia.profesor.correo != null) {
            holder.email_no_asignado.setText(materia.profesor.correo);
            //codigo pal correo
        } else {
            holder.btn_email.setVisibility(View.GONE);
        }

        if (materia.profesor.telefono != null) {
            holder.telef_no_asignado.setText(materia.profesor.telefono);
            //codigo pal correo
        } else {
            holder.btn_llamr.setVisibility(View.GONE);
            holder.btn_whats.setVisibility(View.GONE);
        }

        holder.cardView.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), DetallesDocenteActivity.class);
            intent.putExtra("Nombre", materia.profesor.name);
            intent.putExtra("Iniciales", letras);
            intent.putExtra("Numero", materia.profesor.telefono);
            intent.putExtra("Email", materia.profesor.correo);
            intent.putExtra("Materia", materia.name);
            intent.putExtra("idMateria", materia.id);
            intent.putExtra("Color", materia.color);
            v.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return materias.size();
    }

    static class DocenteViewHolder extends RecyclerView.ViewHolder {
        TextView profesor_nombre;
        TextView profesor_iniciales;
        TextView materia_nombre;
        TextView email_no_asignado;
        TextView telef_no_asignado;
        MaterialButton btn_llamr;
        MaterialButton btn_whats;
        MaterialButton btn_email;

        View divider2;
        ImageView iniciales_bk;
        MaterialCardView cardView;

        public DocenteViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardview_docente);

            divider2 = itemView.findViewById(R.id.cardview_docente_divider2);
            iniciales_bk = itemView.findViewById(R.id.docente_circle_background_iniciales);
            profesor_nombre = itemView.findViewById(R.id.nombre_profesor_cardview);
            profesor_iniciales = itemView.findViewById(R.id.circle_inicial_profesor_cardview);
            materia_nombre = itemView.findViewById(R.id.materia_profesor_cardview);
            email_no_asignado = itemView.findViewById(R.id.email_no_asignado);
            telef_no_asignado = itemView.findViewById(R.id.telef_no_asignado);

            btn_llamr = itemView.findViewById(R.id.btn_llamar);
            btn_whats = itemView.findViewById(R.id.btn_whats);
            btn_email = itemView.findViewById(R.id.btn_email);
        }
    }
}
