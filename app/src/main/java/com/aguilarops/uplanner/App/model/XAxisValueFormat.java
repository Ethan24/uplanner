package com.aguilarops.uplanner.App.model;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.ValueFormatter;

import java.util.ArrayList;

public class XAxisValueFormat extends ValueFormatter {

    private ArrayList<String> dias = new ArrayList<String>() {{
        add("Dom");
        add("Lun");
        add("Mar");
        add("Mie");
        add("Jue");
        add("Vie");
        add("Sab");
    }};

    @Override
    public String getAxisLabel(float value, AxisBase axis) {
        return dias.get((int) value);
    }
}
