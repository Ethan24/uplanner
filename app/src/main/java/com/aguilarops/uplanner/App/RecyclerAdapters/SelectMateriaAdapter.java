package com.aguilarops.uplanner.App.RecyclerAdapters;

import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aguilarops.uplanner.App.Activities.Docentes.EditDocenteActivity;
import com.aguilarops.uplanner.R;
import com.aguilarops.uplanner.abstraction.appdata.materias.Materia;

import java.util.ArrayList;

public class SelectMateriaAdapter extends RecyclerView.Adapter<SelectMateriaAdapter.ItemViewHolder> {

    ArrayList<Materia> materias;
    EditDocenteActivity parent;

    public SelectMateriaAdapter(ArrayList<Materia> materias, EditDocenteActivity parent) {
        this.materias = materias;
        this.parent = parent;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_materia_select, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, final int position) {
        holder.nombre_materia.setText(materias.get(position).name);
        holder.nombre_materia.setCompoundDrawableTintList(ColorStateList.valueOf(materias.get(position).color));
        holder.nombre_materia.setOnClickListener(v -> parent.materia_change(materias.get(position)));
    }

    @Override
    public int getItemCount() {
        return materias.size();
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView nombre_materia;

        ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            this.nombre_materia = itemView.findViewById(R.id.nombre_materia_new_asig);
        }
    }

}
