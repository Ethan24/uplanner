package com.aguilarops.uplanner.App.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aguilarops.uplanner.App.RecyclerAdapters.home.HomeFragmentAdapter;
import com.aguilarops.uplanner.R;
import com.aguilarops.uplanner.abstraction.appdata.materias.Bloque;
import com.github.clans.fab.FloatingActionMenu;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;

public class HomeFragment extends Fragment {

    private ArrayList<Bloque> bloques;

    public HomeFragment(ArrayList<Bloque> bloques) {
        this.bloques = bloques;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.reciclerviewHome);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        FloatingActionMenu fabMenu = view.findViewById(R.id.fab_home);
        fabMenu.setClosedOnTouchOutside(true);

        HomeFragmentAdapter adapter = new HomeFragmentAdapter(bloques, createChartData());
        recyclerView.setAdapter(adapter);
        recyclerView.setItemViewCacheSize(15);
        return view;
    }

    private LineData createChartData() {
        ArrayList<Entry> values = new ArrayList<>();

        values.add(new Entry(0, 0));
        values.add(new Entry(1, 5));
        values.add(new Entry(2, 1));
        values.add(new Entry(3, 3));
        values.add(new Entry(4, 0));
        values.add(new Entry(5, 0));
        values.add(new Entry(6, 2));

        LineDataSet dataSet = new LineDataSet(values,"");
        dataSet.setColor(getResources().getColor(R.color.colorAccent));
        dataSet.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
        dataSet.setCircleColor(getResources().getColor(R.color.colorAccent));
        dataSet.setCubicIntensity(0.08f);
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(dataSet);

        return new LineData(dataSets);
    }
}