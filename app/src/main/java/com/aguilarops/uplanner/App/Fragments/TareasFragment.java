package com.aguilarops.uplanner.App.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aguilarops.uplanner.App.RecyclerAdapters.TareasFragmentAdapter;
import com.aguilarops.uplanner.R;

public class TareasFragment extends Fragment {


    public TareasFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_tareas, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recicyclerview_tareas_frag);
        TareasFragmentAdapter adapter = new TareasFragmentAdapter();
        recyclerView.setAdapter(adapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        return view;
    }
}