package com.aguilarops.uplanner.App.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aguilarops.uplanner.App.RecyclerAdapters.DocentesFragmentAdapter;
import com.aguilarops.uplanner.R;
import com.aguilarops.uplanner.bd.bdProfesor;

public class DocentesFragment extends Fragment {

    public DocentesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_docentes, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view_docentes);
        DocentesFragmentAdapter adapter = new DocentesFragmentAdapter(bdProfesor.getProfesores2show(getContext()));
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        return view;
    }
}