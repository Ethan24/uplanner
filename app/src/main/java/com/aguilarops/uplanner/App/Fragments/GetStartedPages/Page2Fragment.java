package com.aguilarops.uplanner.App.Fragments.GetStartedPages;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.aguilarops.uplanner.App.Activities.HomeActivity;
import com.aguilarops.uplanner.R;
import com.aguilarops.uplanner.util.filechooser.utils.Utility;
import com.aguilarops.uplanner.util.filechooser.view.FilePickerActivity;
import com.google.android.material.button.MaterialButton;

import static android.content.ContentValues.TAG;

public class Page2Fragment extends Fragment {

    private static final int EXTERNAL_READ_PERMISSION_GRANT = 112;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_page2, container, false);

        MaterialButton btn_saltar = view.findViewById(R.id.page2_btn_saltar);
        btn_saltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requireActivity().startActivity(new Intent(requireContext(), HomeActivity.class));
            }
        });

        final MaterialButton btn_cargar = view.findViewById(R.id.page2_btn_cargar);
        btn_cargar.setOnClickListener(v -> {
            if (!Utility.checkStorageAccessPermissions(getContext())) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission
                            .READ_EXTERNAL_STORAGE}, EXTERNAL_READ_PERMISSION_GRANT);
                    int[] results = new int[1];
                    results[0] = -1;
                    onRequestPermissionsResult(EXTERNAL_READ_PERMISSION_GRANT, new String[]{Manifest.permission
                            .READ_EXTERNAL_STORAGE}, results);
                }
            } else {
                requireActivity().startActivity(new Intent(requireContext(), FilePickerActivity.class));
            }
        });
        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == EXTERNAL_READ_PERMISSION_GRANT) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
                requireActivity().startActivity(new Intent(requireContext(), FilePickerActivity.class));
            }
        }
    }
}