package com.aguilarops.uplanner.App.Activities.FillData;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.aguilarops.uplanner.App.Fragments.GetStartedPages.Page1Fragment;
import com.aguilarops.uplanner.App.Fragments.GetStartedPages.Page2Fragment;
import com.aguilarops.uplanner.R;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import java.util.ArrayList;
import java.util.List;

public class LoadUserDataActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_user_data);

        ViewPager viewPager = findViewById(R.id.viepager_load_userData);
        AdapterViewPager adapterViewPager = new AdapterViewPager(getSupportFragmentManager(),
                FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPager.setAdapter(adapterViewPager);

        DotsIndicator dotsIndicator = findViewById(R.id.dots_loadUserDataActivity);
        dotsIndicator.setViewPager(viewPager);

    }

    @Override
    public void onBackPressed() {
    }

    static class AdapterViewPager extends FragmentPagerAdapter {

        private final List<Fragment> listaDeFragmentos = new ArrayList<>();

        public AdapterViewPager(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
            listaDeFragmentos.add(new Page1Fragment());
            listaDeFragmentos.add(new Page2Fragment());
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return listaDeFragmentos.get(position);
        }

        @Override
        public int getCount() {
            return listaDeFragmentos.size();
        }

    }
}