package com.aguilarops.uplanner.App.Fragments;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.aguilarops.uplanner.App.Activities.FillData.DataLoadedActivity;
import com.aguilarops.uplanner.R;
import com.aguilarops.uplanner.abstraction.appdata.materias.Bloque;
import com.aguilarops.uplanner.bd.bdMateria;
import com.aguilarops.uplanner.util.Util;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.chip.Chip;
import com.thebluealliance.spectrum.SpectrumDialog;

import java.util.ArrayList;


public class DataLoadedFragment extends Fragment {

    private Bloque data;
    private View view;
    private int color_materia, numPage;
    private MaterialButton ColorPickerbutton;
    private ArrayList<Chip> chips;

    public DataLoadedFragment(int color, int numPage, Bloque bloque) {
        this.color_materia = color;
        this.numPage = numPage;
        data = bloque;
        chips = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_data_loaded, container, false);
        createChips(data.materia.dias.toArray(new Integer[0]));

        TextView nombre = view.findViewById(R.id.nombre_asignatura);
        nombre.setText(data.materia.name);
        TextView grupo = view.findViewById(R.id.txt_grupo);
        grupo.setText(data.materia.grupo);
        TextView edificio = view.findViewById(R.id.EdificioInput);
        edificio.setText(data.edificio);
        TextView seccion = view.findViewById(R.id.SalonInput);
        seccion.setText(data.seccion);
        TextView nombre_docente = view.findViewById(R.id.docenteInput);
        nombre_docente.setText(data.materia.profesor.name);
        EditText num_docente = view.findViewById(R.id.phoneInput);
        num_docente.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String txt = s.toString();
                if (txt.length() >= 8) {
                    if (txt.contains("+")) {
                        if (txt.indexOf("+") == txt.lastIndexOf("+")) {
                            txt = txt.replace("+", "");
                        }
                    }

                    if (Util.isNumeric(txt.replace(" ", ""))) {
                        ((DataLoadedActivity) requireActivity()).addNumber(s.toString(), numPage);
                    }
                }
            }
        });

        EditText email_docente = view.findViewById(R.id.EmailInput);
        email_docente.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String txt = s.toString();
                if (txt.contains("@") && txt.contains(".")) {
                    if (txt.lastIndexOf(".") > txt.lastIndexOf("@") && txt.lastIndexOf(".") < txt.length() - 1) {
                        if (Patterns.EMAIL_ADDRESS.matcher(txt).matches()) {
                            ((DataLoadedActivity) requireActivity()).addEmail(txt, numPage);
                        }
                    }
                }
            }
        });

        ColorPickerbutton = view.findViewById(R.id.colorPickerCircle);
        ColorPickerbutton.setBackgroundColor(color_materia);
        ColorPickerbutton.setOnClickListener(v -> {

            SpectrumDialog dialog = new SpectrumDialog.Builder(requireContext())
                    .setColors(requireActivity().getResources()
                            .getIntArray(R.array.color_pallete))
                    .setSelectedColor(color_materia)
                    .build();

            dialog.show(requireActivity().getSupportFragmentManager(), "ColorPicker");
            dialog.setOnColorSelectedListener((positiveResult, color) -> {
                color_materia = color;
                bdMateria.materia_set_color(requireActivity().getApplicationContext(), data.materia.id, color);
                ColorPickerbutton.setBackgroundTintList(ColorStateList.valueOf(color));
                checkChips();
            });
        });

        return view;
    }

    private void checkChips() {
        if (chips.size() > 0)
            for (Chip i : chips)
                i.setChipBackgroundColor(ColorStateList.valueOf(color_materia));

    }

    private void createChips(Integer[] days) {
//        System.out.println(data.materia.name);
        for (int i : days) {
//            System.out.println(i);
            switch (i) {
                case 0:
                    chips.add(view.findViewById(R.id.chip_dom));
                    break;
                case 1:
                    chips.add(view.findViewById(R.id.chip_lun));
                    break;
                case 2:
                    chips.add(view.findViewById(R.id.chip_mar));
                    break;
                case 3:
                    chips.add(view.findViewById(R.id.chip_mie));
                    break;
                case 4:
                    chips.add(view.findViewById(R.id.chip_jue));
                    break;
                case 5:
                    chips.add(view.findViewById(R.id.chip_vie));
                    break;
                case 6:
                    chips.add(view.findViewById(R.id.chip_sab));
                    break;
                default:
                    break;
            }
        }

        checkChips();
    }
}