package com.aguilarops.uplanner.App.Activities.Docentes;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aguilarops.uplanner.App.Activities.Materias.EditMateriaActivity;
import com.aguilarops.uplanner.App.RecyclerAdapters.SelectMateriaAdapter;
import com.aguilarops.uplanner.R;
import com.aguilarops.uplanner.abstraction.appdata.materias.Materia;
import com.aguilarops.uplanner.bd.bdMateria;
import com.aguilarops.uplanner.util.Util;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import java.util.ArrayList;
import java.util.Objects;

public class EditDocenteActivity extends AppCompatActivity {
    TextView docente_materia;
    AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_docente);

        if (Objects.requireNonNull(getIntent().getStringExtra(Activity.ACTIVITY_SERVICE)).equalsIgnoreCase(EditMateriaActivity.class.getSimpleName())) {
            Toast.makeText(this, "uwu", Toast.LENGTH_LONG).show();
        }

        MaterialToolbar toolbar = findViewById(R.id.toolbar_edit_docente);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        EditText docente_nombre_tx = findViewById(R.id.editar_docente_nombre_tx);
        docente_nombre_tx.setText(getIntent().getStringExtra("Nombre"));
        docente_nombre_tx.addTextChangedListener(new TextWatcher() {
            String first = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                first = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                String _new = s.toString();
                if (!_new.isEmpty()) {
                    if (first.isEmpty()) {
                        s.replace(0, _new.length(), _new.replace(" ", ""));
                    }
                    if (Util.getcount(_new, ' ') > 1 || !_new.matches("[a-zA-Z ]+")) {
                        s.replace(0, _new.length(), first);
                    }
                }
            }
        });

        EditText docente_numero_tx = findViewById(R.id.editar_docente_numero_tx);
        String aux = getIntent().getStringExtra("Numero");
        if (aux != null)
            docente_numero_tx.setText(aux);

        EditText docente_email_tx = findViewById(R.id.editar_docente_email_tx);
        aux = getIntent().getStringExtra("Email");
        if (aux != null)
            docente_email_tx.setText(aux);


        docente_materia = findViewById(R.id.editar_docente_materia);
        docente_materia.setText(getIntent().getStringExtra("Materia"));

        ExtendedFloatingActionButton floatingActionButton = findViewById(R.id.fb_edit_docente);
        floatingActionButton.setOnClickListener(v -> {
            String phone = docente_numero_tx.getText().toString();
            boolean isValid_email = Util.isValid_email(docente_email_tx.getText().toString());
            boolean isValid_phone = Util.isValid_phone(docente_numero_tx.getText().toString());
            String message = null;
            if (!isValid_email && !isValid_phone) {
                message = "Correo y telefono invalidos";
            } else if (!isValid_email) {
                message = "Correo invalido";
            } else if (!isValid_phone) {
                message = "Telefono invalido";
            }

            if (isValid_phone) {

            }
            if (message != null)
                Toast.makeText(EditDocenteActivity.this, message, Toast.LENGTH_SHORT).show();
        });

    }


    public void selectMateria(View v) {
        ArrayList<Materia> materias = bdMateria.getList_of_Materias2Select(getApplicationContext());

        @SuppressLint("InflateParams")
        View view1 = LayoutInflater.from(this).inflate(R.layout.simple_recycler, null);
        RecyclerView recyclerView = view1.findViewById(R.id.reciclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(view1.getContext()));
        SelectMateriaAdapter adapter = new SelectMateriaAdapter(materias, EditDocenteActivity.this);
        recyclerView.setAdapter(adapter);


        dialog = new MaterialAlertDialogBuilder(this, R.style.AlertDialog)
                .setTitle("Asignatura")
                .setView(view1)
                .setCancelable(true)
                .setPositiveButton("Crear Nueva", (dialog, which) -> Toast.makeText(getBaseContext(), "Pressed", Toast.LENGTH_SHORT).show()).show();
    }

    public void materia_change(Materia result) {
//        System.out.println("yes");
        if (result != null) {
//            System.out.println("yes2");
            docente_materia.setText(result.name);
            ImageView color = findViewById(R.id.materia_circle_color);
            color.setImageTintList(ColorStateList.valueOf(result.color));
            dialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishActivity(0);
    }
}