package com.aguilarops.uplanner.App.RecyclerAdapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aguilarops.uplanner.R;

public class TareasFragmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public TareasFragmentAdapter() {

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate((viewType == 1) ?
                R.layout.fragment_tareas_item : R.layout.fragment_tareas_item_empty, parent, false);
        return (viewType == 1) ?
                new TareasFragmentAdapter.ItemViewHolder(view) :
                new TareasFragmentAdapter.EmptyItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 8;
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        LinearLayout color_event, item_bk;
        TextView nombre_evento, hora_evento;

        ItemViewHolder(@NonNull View itemView) {
            super(itemView);
        /*  this.item_bk = itemView.findViewById(R.id.card_home_eventos_item);
            this.color_event = itemView.findViewById(R.id.color_evento);

            this.nombre_evento = itemView.findViewById(R.id.nombre_evento_item);
            this.hora_evento = itemView.findViewById(R.id.hora_evento_item);*/
        }
    }

    static class EmptyItemViewHolder extends RecyclerView.ViewHolder {
        EmptyItemViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}