package com.aguilarops.uplanner.App.RecyclerAdapters.home;

import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aguilarops.uplanner.R;
import com.aguilarops.uplanner.abstraction.appdata.materias.Bloque;
import com.aguilarops.uplanner.util.Util;

import java.util.ArrayList;

public class HorarioDiarioAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Bloque> bloques;

    public HorarioDiarioAdapter(ArrayList<Bloque> bloques) {
        this.bloques = bloques;
    }

    @Override
    public int getItemCount() {
        return bloques.size() > 0 ? bloques.size() : 1;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate((bloques.size() > 0 ? R.layout.home_horario_diario_item : R.layout.home_horario_diario_item_empty),
                        parent, false);

        return new MateriaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (getItemViewType(position) == 1) {
            Bloque bloque = bloques.get(position);
            MateriaViewHolder mholder = (MateriaViewHolder) holder;
            mholder.nombre_tx.setText(bloques.get(position).materia.name);
            mholder.color.setImageTintList(ColorStateList.valueOf(bloques.get(position).materia.color));
            mholder.edificio_tx.setText(bloques.get(position).edificio);
            mholder.seccion_tx.setText(bloques.get(position).seccion);
            String horas = Util.getHoraFormat_from24(bloque.inicio.horas, bloque.inicio.minutos) +
                    " - " + Util.getHoraFormat_from24(bloque.fin.horas, bloque.fin.minutos);
            mholder.hora_inicio_fin_tx.setText(horas);
        }

    }


    @Override
    public int getItemViewType(int position) {
        return bloques.size() > 0 ? 1 : 2;
    }

    static class MateriaViewHolder extends RecyclerView.ViewHolder {
        TextView nombre_tx, edificio_tx, seccion_tx, hora_inicio_fin_tx;
        ImageView color;

        public MateriaViewHolder(View view) {
            super(view);
            this.color = view.findViewById(R.id.color_asignatura_home_horario_item);
            this.nombre_tx = view.findViewById(R.id.nombre_asignatura_tx);
            this.edificio_tx = view.findViewById(R.id.edificio_tx);
            this.seccion_tx = view.findViewById(R.id.seccion_tx);
            this.hora_inicio_fin_tx = view.findViewById(R.id.hora_inicio_chip);

        }
    }
}
