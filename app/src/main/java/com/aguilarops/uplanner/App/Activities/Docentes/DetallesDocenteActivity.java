package com.aguilarops.uplanner.App.Activities.Docentes;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.aguilarops.uplanner.R;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class DetallesDocenteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_docente);

        MaterialToolbar toolbar = findViewById(R.id.detalle_docente_toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        toolbar.setTitle(getIntent().getStringExtra("Nombre"));

        ImageView circle_iniciales = findViewById(R.id.det_docente_circle_background_iniciales);
        circle_iniciales.setImageTintList(ColorStateList.valueOf(getIntent().getIntExtra("Color",
                getResources().getColor(R.color.colorAccent, null))));

        TextView nombre_tx, telefono_tx, email_tx, materia_tx, iniciales_tx;

        iniciales_tx = findViewById(R.id.det_docente_iniciales);
        iniciales_tx.setText(getIntent().getStringExtra("Iniciales"));

        nombre_tx = findViewById(R.id.det_docente_nombre_tx);
        nombre_tx.setText(getIntent().getStringExtra("Nombre"));


        telefono_tx = findViewById(R.id.det_docente_numero_tx);
        if (getIntent().getStringExtra("Numero") != null)
            telefono_tx.setText(getIntent().getStringExtra("Numero"));


        email_tx = findViewById(R.id.det_docente_email_tx);
        if (getIntent().getStringExtra("Email") != null)
            email_tx.setText(getIntent().getStringExtra("Email"));

        materia_tx = findViewById(R.id.det_docente_materia_tx);
        materia_tx.setText(getIntent().getStringExtra("Materia"));

        FloatingActionButton floatingActionButton = findViewById(R.id.fab_detalles_docente);
        floatingActionButton.setBackgroundTintList(ColorStateList.valueOf(getIntent().getIntExtra("Color",
                getResources().getColor(R.color.colorAccent, null))));

        floatingActionButton.setOnClickListener(v -> {
            Intent intent = new Intent(DetallesDocenteActivity.this, EditDocenteActivity.class);
            intent.putExtra("Nombre", getIntent().getStringExtra("Nombre"));
            intent.putExtra("Numero", getIntent().getStringExtra("Numero"));
            intent.putExtra("Email", getIntent().getStringExtra("Email"));
            intent.putExtra("Materia", getIntent().getStringExtra("Materia"));
            intent.putExtra("idMateria", getIntent().getStringExtra("idMateria"));
            startActivity(intent);
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishActivity(0);
    }
}