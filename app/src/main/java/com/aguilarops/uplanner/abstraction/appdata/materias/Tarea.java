package com.aguilarops.uplanner.abstraction.appdata.materias;

public class Tarea {
    public String id;
    public String desc;
    public String fecha;
    public Materia materia;
}
