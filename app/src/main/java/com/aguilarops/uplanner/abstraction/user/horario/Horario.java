package com.aguilarops.uplanner.abstraction.user.horario;

import java.util.Calendar;
import java.util.Date;

/**
 * @author Jeremmy
 */
public class Horario {
    private static boolean full = false;
    private static boolean succes = false;


    public static boolean isFull() {
        return full;
    }

    public static boolean isSucces() {
        return succes;
    }

    public static void setSucces() {
        Horario.succes = true;
    }

    public static void setFail() {
        Horario.succes = false;
    }

    public static void setFull() {
        full = true;
    }

    public static void setVoid() {
        full = false;
    }

    public static int getDay() {
        Date now = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);

        return calendar.get(Calendar.DAY_OF_WEEK) - 1;
    }
}
