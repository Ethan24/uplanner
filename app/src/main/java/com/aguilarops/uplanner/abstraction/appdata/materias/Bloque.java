package com.aguilarops.uplanner.abstraction.appdata.materias;

import com.aguilarops.uplanner.util.Tiempo;

public class Bloque {
    public String id;
    public Tiempo inicio;
    public Tiempo fin;
    public String edificio;
    public String seccion;
    public Materia materia;
    public Integer day;

    public Bloque() {
    }

    public Bloque(String id, Tiempo inicio, Tiempo fin, String edificio, String seccion, Materia materia, Integer day) {
        this.id = id;
        this.inicio = inicio;
        this.fin = fin;
        this.edificio = edificio;
        this.seccion = seccion;
        this.materia = materia;
        this.day = day;
    }

    public Bloque(Tiempo inicio, Tiempo fin, String edificio, String seccion, String idmateria, Integer day) {
        this.inicio = inicio;
        this.fin = fin;
        this.edificio = edificio;
        this.seccion = seccion;
        this.materia = new Materia();
        this.materia.id = idmateria;
        this.day = day;
    }

    public Bloque(Tiempo inicio, Tiempo fin, String edificio, String seccion, Materia materia, Integer day) {
        this.inicio = inicio;
        this.fin = fin;
        this.edificio = edificio;
        this.seccion = seccion;
        this.materia = materia;
        this.day = day;
    }

    public Bloque(Tiempo inicio, Tiempo fin, Integer day) {
        this.inicio = inicio;
        this.fin = fin;
        this.materia = null;
        this.day = day;
    }
}
