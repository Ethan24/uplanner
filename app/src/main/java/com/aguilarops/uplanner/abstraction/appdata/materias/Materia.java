package com.aguilarops.uplanner.abstraction.appdata.materias;

import java.util.ArrayList;

public class Materia {
    public String id;
    public String name;
    public int color;
    public Profesor profesor;
    public String grupo;
    public String nextClass;
    public ArrayList<Integer> dias;

    public Materia() {
        dias = new ArrayList<>();
    }

}
