package com.aguilarops.uplanner.abstraction.appdata.materias;

public class Profesor {
    public String id;
    public String name;
    public String telefono;
    public String correo;

    public Profesor() {
    }

    public Profesor(String id, String name, String telefono) {
        this.id = id;
        this.name = name;
        this.telefono = telefono;
    }
}
