package com.aguilarops.uplanner.PDF;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.RectF;

import com.aguilarops.uplanner.abstraction.appdata.materias.Bloque;
import com.aguilarops.uplanner.abstraction.user.horario.Horario;
import com.aguilarops.uplanner.bd.bdUser;
import com.aguilarops.uplanner.util.Tiempo;
import com.tom_roush.pdfbox.pdmodel.PDDocument;
import com.tom_roush.pdfbox.pdmodel.PDPage;
import com.tom_roush.pdfbox.text.PDFTextStripperByArea;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.aguilarops.uplanner.bd.bdBloque.registrar_bloque;
import static com.aguilarops.uplanner.bd.bdBloque.registrar_empty_bloque;
import static com.aguilarops.uplanner.bd.bdMateria.getMateria_id;
import static com.aguilarops.uplanner.bd.bdMateria.materia_exist;
import static com.aguilarops.uplanner.bd.bdMateria.registrar_materia;
import static com.aguilarops.uplanner.bd.bdProfesor.getProfesor_id;
import static com.aguilarops.uplanner.bd.bdProfesor.profesor_exist;
import static com.aguilarops.uplanner.bd.bdProfesor.registrar_profesor;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

public class PDFManager {

    private float MM_TO_UNITS = 1 / (10 * 2.54f) * 72;

    private Periodo m1 = new Periodo();
    private Periodo m2 = new Periodo();
    private Periodo m3 = new Periodo();

    private int getDocumentPageCount;
    private PDDocument document;
    private TDia Lunes;
    private TDia Martes;
    private TDia Miercoles;
    private TDia Jueves;
    private TDia Viernes;
    private Context c;

    public PDFManager(Context c, String filepath) {
        this.c = c;
        //Toast.makeText(c ,filepath, Toast.LENGTH_LONG).show();
        Lunes = new TDia(25, 40.6f, 1);
        Martes = new TDia(60, 40.6f, 2);
        Miercoles = new TDia(100, 40.6f, 3);
        Jueves = new TDia(140, 35.6f, 4);
        Viernes = new TDia(175, 45, 5);

        document = null;
        if (filepath.trim().endsWith(".pdf")) {
//            Toast.makeText(c ,filepath, Toast.LENGTH_LONG).show();
            File file = new File(filepath);
            try {
                // PDFParser parser = new PDFParser(new RandomAccessFile(file, "r")); // update for PDFBox V 2.0
                // parser.parse();
                // COSDocument cosDoc = parser.getDocument();

                // document = new PDDocument(cosDoc);

                document = PDDocument.load(file);

                getDocumentPageCount = document.getNumberOfPages();
                //  System.out.println(getDocumentPageCount);
            } catch (IOException ex) {
                Logger.getLogger(PDFManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }  //  System.out.println("No data return");
    }

    private static String Numeros2Romanos(int numero) {
        int i, decenas, unidades;
        String romano = "";

        //obtenemos cada cifra del número
        decenas = numero / 10 % 10;
        unidades = numero % 10;

        //decenas
        if (decenas == 9) {
            romano = romano + "XC";
        } else if (decenas >= 5) {
            romano = romano + "L";
            for (i = 6; i <= decenas; i++) {
                romano = romano.concat("X");
            }
        } else if (decenas == 4) {
            romano = romano + "XL";
        } else {
            for (i = 1; i <= decenas; i++) {
                romano = romano.concat("X");
            }
        }

        //unidades
        if (unidades == 9) {
            romano = romano + "IX";
        } else if (unidades >= 5) {
            romano = romano + "V";
            for (i = 6; i <= unidades; i++) {
                romano = romano.concat("I");
            }
        } else if (unidades == 4) {
            romano = romano + "IV";
        } else {
            for (i = 1; i <= unidades; i++) {
                romano = romano.concat("I");
            }
        }
        return romano;
    }

    private char[] tolower(char[] uwu) {
        for (int i = 0; i < uwu.length; i++) {
            uwu[i] = Character.toLowerCase(uwu[i]);
        }

        return uwu;
    }

    private String trate_hora(String txt) {
        if (!txt.contains("p.m") && txt.contains("a.m")) {
            txt = txt.substring(0, txt.indexOf("a.m"));
            txt += "am";
        } else if (txt.contains("p.m") && !txt.contains("a.m")) {
            txt = txt.substring(0, txt.indexOf("p.m"));
            txt += "pm";
        }

        return txt;
    }

    private RectF new_rect(float x, float y, float w, float h) {
        return new RectF(x, y, x + w, y + h);
    }

    private String findHora(PointF rp, float width, float time_period_height, float line_height, int page) {
        String txt = "";
        RectF hora;
        while (true) {
            hora = new_rect(rp.x * MM_TO_UNITS, rp.y * MM_TO_UNITS, width * MM_TO_UNITS, time_period_height * MM_TO_UNITS);
            try {
                txt = getTextUsingCoordinates(page, hora);
            } catch (Exception ex) {
                Logger.getLogger(c.getClass().getName()).log(Level.SEVERE, null, ex);
            }
            txt = txt.trim();

            if (txt.length() > 11) {
                break;
            }

            if (rp.y >= 151) {
                return null;
            }

            rp.y += line_height;
        }

        return txt;
    }

    private void DEf_Horas(int page) {
        String text = "uwu";

        float line_height = 4f;
        float time_period_height = 4.3f * 2;
        float minim_height = line_height * 5;
        PointF rp = new PointF(10, 52.51f);//54.51
        float width = 10;

        float total_Readable = 146.49f;

        m1.x = rp.x;
        m1.y = rp.y;

        RectF hora = new_rect(rp.x * MM_TO_UNITS, rp.y * MM_TO_UNITS, width * MM_TO_UNITS, time_period_height * MM_TO_UNITS);

        try {
            text = getTextUsingCoordinates(page, hora);
        } catch (Exception ex) {
            Logger.getLogger(c.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        //  System.out.println(text);

        m1.inicio = trate_hora(text.substring(0, text.indexOf('\n')));
        m1.fin = trate_hora(text.substring(text.indexOf('\n') + 1));

        rp.y += minim_height;

        text = findHora(rp, width, time_period_height, line_height, page);

        if (text == null) {
            m2.inicio = null;
            m3.inicio = null;
        } else {
            m2.x = rp.x;
            m2.y = rp.y;
            System.out.println(text + " (" + m2.x + ", " + m2.y + ")");
            //System.out.println(rp.y);
            rp.y += minim_height;

            m2.inicio = trate_hora(text.substring(0, text.indexOf('\n')));
            m2.fin = trate_hora(text.substring(text.indexOf('\n') + 1));

            text = findHora(rp, width, time_period_height, line_height, page);
            if (text == null) {
                m3.inicio = null;
            } else {
                m3.x = rp.x;
                m3.y = rp.y;
                System.out.println(text + " (" + m3.x + ", " + m3.y + ")");

                rp.y += minim_height;
                m3.inicio = trate_hora(text.substring(0, text.indexOf('\n')));
                m3.fin = trate_hora(text.substring(text.indexOf('\n') + 1));
            }
        }

        if (m2.inicio == null) {
            m1.height = total_Readable - m1.y;
        } else {
            m1.height = m2.y - m1.y;
            if (m3.inicio == null) {
                m2.height = total_Readable - m2.y;
            } else {
                m2.height = m3.y - m2.y;
                m3.height = total_Readable - m2.height - m1.height;
                System.out.println("puto");
            }
        }

//        ftext = "Primer: " + m1.inicio + " / " + m1.fin
//                + "\nSegundo: " + m2.inicio + " / " + m2.fin
//                + "\nTercero: " + m3.inicio + " / " + m3.fin;
    }

    private List<Integer> extraerNumeros(String cadena) {
        List<Integer> todosLosNumeros = new ArrayList<>();
        Matcher encuentrador = Pattern.compile("\\d+").matcher(cadena);
        while (encuentrador.find()) {
            todosLosNumeros.add(Integer.parseInt(encuentrador.group()));
        }
        return todosLosNumeros;
    }

    private String clasFormat(String t) {
        if (t.contains(" De ")) {
            t = t.replace(" De ", " de ");
        }

        if (t.contains(" La ")) {
            t = t.replace(" La ", " la ");
        }

        if (t.contains(" A ")) {
            t = t.replace(" A ", " a ");
        }

        List<Integer> n = extraerNumeros(t);

        if (n.size() > 0) {
            t = t.substring(0, t.indexOf(n.get(0).toString()));
            t += Numeros2Romanos(n.get(0));
        }
//      //  System.out.println(t);

        return t;
    }

    private String nojumps(String t) {
        return t.replace("\n", "").replace("\r", "").trim();
    }

    private ArrayList<String> getClassData(String txt) {
        ArrayList<String> data = new ArrayList<>();
        String materia;
        String grupo;
        String edificio;
        String aula;
        String docente;

        materia = clasFormat(convierte_formato(txt.substring(0, txt.indexOf(",")))).trim();

        grupo = nojumps(txt.substring(txt.indexOf("Grupo: ") + "Grupo: ".length(), txt.indexOf("Aula")).trim());

        edificio = convierte_formato(txt.substring(txt.indexOf("Edificio: ") + "Edificio: ".length(), txt.indexOf("Docente")));
        if (!edificio.contains("Edificio")) {
            edificio = "Edificio " + edificio;
        }

        aula = nojumps(txt.substring(txt.indexOf("Aula: ") + "Aula: ".length(), txt.indexOf("Edificio")));

        docente = convierte_formato(txt.substring(txt.indexOf("Docente: ") + "Docente: ".length()));

//      //  System.out.println("-----------");
//      //  System.out.print(docente);
        data.add(materia);
        data.add(grupo);
        data.add(aula);
        data.add(edificio);
        data.add(docente);

        ////System.out.println(data);
        return data;
    }

    private String convierte_formato(String string) {
        if (string == null) {
            return null;
        }
        string = nojumps(string).trim();

        String romanos = "";

        if (string.contains(" I") && string.indexOf(" I") + " I".length() == string.length()) {
            string = string.replace(" I", "");
            romanos = "I";
        }

        if (string.contains(" II") && string.indexOf(" II") + " II".length() == string.length()) {
            string = string.replace(" II", "");
            romanos = "II";
        }

        if (string.contains(" III") && string.indexOf(" III") + " III".length() == string.length()) {
            string = string.replace(" III", "");
            romanos = "III";
        }

        if (string.contains(" IV") && string.indexOf(" IV") + " IV".length() == string.length()) {
            string = string.replace(" IV", "");
            romanos = "IV";
        }

        if (string.contains(" V") && string.indexOf(" V") + " V".length() == string.length()) {
            string = string.replace(" V", "");
            romanos = "V";
        }

        if (string.contains(" VI") && string.indexOf(" VI") + " VI".length() == string.length()) {
            string = string.replace(" VI", "");
            romanos = "VI";
        }

        if (string.contains(" VII") && string.indexOf(" VII") + " VII".length() == string.length()) {
            string = string.replace(" VII", "");
            romanos = "VII";
        }

        StringBuilder builder = new StringBuilder();
        StringTokenizer st = new StringTokenizer(string, " ");
        while (st.hasMoreElements()) {
            String ne = (String) st.nextElement();
            if (ne.length() > 0) {
                builder.append(ne.substring(0, 1).toUpperCase());
                builder.append(ne.substring(1).toLowerCase()); //agregado
                builder.append(' ');
            }
        }

        return builder.toString().concat(romanos).trim();
    }

    private void getDay_box(TDia dia, Periodo p, int page) {
        ArrayList<String> data;
        RectF section = new_rect(dia.x * MM_TO_UNITS, p.y * MM_TO_UNITS, dia.width * MM_TO_UNITS, p.height * MM_TO_UNITS);
        String text = " ";
        Bloque bloque = null;
        String grupo;
        String idMateria;
        String idProfesor;

        try {
            text = getTextUsingCoordinates(page, section);
        } catch (Exception ex) {
            Logger.getLogger(c.getClass().getName()).log(Level.SEVERE, null, ex);
        }


        if (nojumps(text.trim()).replace(" ", "").length() <= 2) {
//            System.out.println("page: " + page);
//            System.out.println(p.inicio + "./." + p.fin + "dia" + dia.n);
//            System.out.println("TEXTO: " + text);
            bloque = new Bloque(new Tiempo(p.inicio), new Tiempo(p.fin), dia.n);
        } else {

            data = getClassData(text);
            //data 0    materia
            //data 1    grupo
            //data 2    seccion
            //data 3    edificio
            //data 4    docente


//            System.out.println(p.inicio + "-/-" + p.fin + "dia" + dia.n + "pagina: " + page);
//            System.out.println(data.get(0));
            String docente = "null";
            if (!profesor_exist(c, data.get(4))) {
                docente = data.get(4).replace(" Del ", " ").replace(" De ", " ")
                        .replace(" Los ", " ").replace(" Las ", " ")
                        .replace(" La ", " ").replace(" El ", " ");
                String[] names = docente.split(" ");
                docente = names[0] + " " + names[2];
                registrar_profesor(c, docente);
            }
            idProfesor = getProfesor_id(c, docente);

            grupo = data.get(1);

            if (grupo != null && idProfesor != null) {
                if (!materia_exist(c, data.get(0).trim())) {
//                    System.out.println(data.get(0));
                    registrar_materia(c, data.get(0), idProfesor, grupo);
                }
            }
            idMateria = getMateria_id(c, data.get(0));
//            System.out.println("idMateria: " + idMateria);

            if (idMateria != null) {
                bloque = new Bloque(new Tiempo(p.inicio), new Tiempo(p.fin), data.get(3), data.get(2), idMateria, dia.n);
            }
        }

        assert bloque != null;
        if (bloque.materia != null) {
            registrar_bloque(c, bloque.inicio, bloque.fin,
                    bloque.edificio, bloque.seccion, bloque.materia.id, bloque.day);
        } else {
            registrar_empty_bloque(c, bloque.inicio, bloque.fin, bloque.day);
        }
    }

    private void day_read(TDia d, int page) {
        if (m1.inicio != null) getDay_box(d, m1, page);
        if (m2.inicio != null) getDay_box(d, m2, page);
        if (m3.inicio != null) getDay_box(d, m3, page);
    }

    public void read_horario() {
        if (isAvalidUNItimeTable()) {
            String name = Student_Name();
            if (name == null) return;
            bdUser.registrarUsuario(c, name);
            if (getDocumentPageCount > 0) {
                int page = 1;

                while (page <= getDocumentPageCount) {
                    DEf_Horas(page);
                    day_read(Lunes, page);
                    day_read(Martes, page);
                    day_read(Miercoles, page);
                    day_read(Jueves, page);
                    day_read(Viernes, page);

                    page++;
                }
            }
            System.out.println("success");
            Horario.setSucces();
        } else {
            Horario.setFail();
            System.out.println("fail");
        }
        Horario.setFull();
//        file.delete();
        System.out.println("full");
    }

    private String Student_Name() {
        RectF section = new_rect(30 * MM_TO_UNITS, 40 * MM_TO_UNITS, 150 * MM_TO_UNITS, 1 * MM_TO_UNITS);
        String text = "uwu";

        try {
            text = getTextUsingCoordinates(1, section);
        } catch (Exception ex) {
            Logger.getLogger(c.getClass().getName()).log(Level.SEVERE, null, ex);
        }

        char[] name = tolower(text.toCharArray());

        name[0] = Character.toUpperCase(name[0]);

        while (true) {
            int i = text.indexOf(' ');

            if (i < 1 || i >= name.length + 1) {
                break;
            }
            text = text.substring(0, i) + "-" + text.substring(i + 1);

            name[i + 1] = Character.toUpperCase(name[i + 1]);
        }

        text = String.copyValueOf(name);

        if (text.length() < "jeremmy".length()) {
            Horario.setFull();
            Horario.setFail();
            return null;
        }

        return text;
    }

    public void close() {
        try {
            document.close();
        } catch (IOException ex) {
            Logger.getLogger(PDFManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String getTextUsingCoordinates(int pageNumber, RectF rect) {
        String extractedText = "";
        ////System.out.println(rect.getY() / MM_TO_UNITS);
        // PDDocument Creates an empty PDF document. You need to add at least
        // one page for the document to be valid.
        // Using load method we can load a PDF document
        PDPage page = new PDPage();
        try {
            // Get specific page. THe parameter is pageindex which starts with // 0. If we need to
            // access the first page then // the pageIdex is 0 PDPage
            if (getDocumentPageCount > 1) {
                page = document.getPage(--pageNumber);
            } else if (getDocumentPageCount >= 0) {
                page = document.getPage(0);
            }
            // To create a rectangle by passing the x axis, y axis, width and height
            String regionName = "region1";

            // Strip the text from PDF using PDFTextStripper Area with the
            // help of Rectangle and named need to given for the rectangle
            PDFTextStripperByArea stripper = new PDFTextStripperByArea();
            stripper.setSortByPosition(false);
            stripper.addRegion(regionName, rect);
            try {
                stripper.extractRegions(page);
            } catch (ArrayIndexOutOfBoundsException ex) {
                return "";
            }
            ////System.out.println("Region is " + stripper.getTextForRegion("region1"));
            extractedText = stripper.getTextForRegion("region1");

        } catch (IOException e) {
            //  System.out.println("The file  not found" + "");
        }
        // Return the extracted text and this can be used for assertion
        return extractedText;
    }

    boolean isAvalidUNItimeTable2() {
        PDPage page = new PDPage();
        try {
            // Get specific page. THe parameter is pageindex which starts with // 0. If we need to
            // access the first page then // the pageIdex is 0 PDPage
            if (getDocumentPageCount > 1) {
                page = document.getPage(0);
            } else if (getDocumentPageCount >= 0) {
                page = document.getPage(0);
            }
            RectF rect = new_rect(92 * MM_TO_UNITS, 16 * MM_TO_UNITS, 110 * MM_TO_UNITS, 5 * MM_TO_UNITS);
            // To create a rectangle by passing the x axis, y axis, width and height
            String regionName = "region1";

            // Strip the text from PDF using PDFTextStripper Area with the
            // help of Rectangle and named need to given for the rectangle
            PDFTextStripperByArea stripper = new PDFTextStripperByArea();
            stripper.setSortByPosition(false);
            stripper.addRegion(regionName, rect);
            if (stripper.getRegions().size() <= 0) return false;
            stripper.extractRegions(page);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("catched java.lang.ArrayIndexOutOfBoundsException");
            return false;
        }
        return true;
    }

    boolean isAvalidUNItimeTable() {
        if (isAvalidUNItimeTable2()) {
            System.out.println("isAvalidUNItimeTable");
            String title = "";
            String subtitle = "";
            RectF t = new_rect(92 * MM_TO_UNITS, 16 * MM_TO_UNITS, 110 * MM_TO_UNITS, 5 * MM_TO_UNITS);
            RectF subt = new_rect(120 * MM_TO_UNITS, 27 * MM_TO_UNITS, 50 * MM_TO_UNITS, 5 * MM_TO_UNITS);

            try {
                title = nojumps(getTextUsingCoordinates(1, t));
            } catch (Exception ex) {
                Logger.getLogger(c.getClass().getName()).log(Level.SEVERE, null, ex);
            }

            try {
                subtitle = nojumps(getTextUsingCoordinates(1, subt));
            } catch (Exception ex) {
                Logger.getLogger(c.getClass().getName()).log(Level.SEVERE, null, ex);
            }

            return subtitle.equals("H O R A R I O") && title.equals("UNIVERSIDAD NACIONAL DE INGENIERÍA");
        } else {
            System.out.println("is valid falso");
            return false;
        }
    }
}


class TDia {

    int n;
    float x;
    float width;

    TDia(float x, float width, int number) {
        this.x = x;
        this.width = width;
        n = number;
    }
}

class Periodo {
    String inicio;
    String fin;
    float x;
    float y;
    float height;

    Periodo() {
        inicio = "";
        fin = "";
    }
}

