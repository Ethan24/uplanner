package com.aguilarops.uplanner.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import static com.aguilarops.uplanner.bd.bdUtil.new_SQlite_Conection;

public class bdTarea {
    static final String TABLA_TAREA = "tarea";

    static final String $TAREA_ID = "tarea_id";
    static final String $TAREA_DESC = "tarea_desc";
    static final String $TAREA_IDMATERIA = "tarea_idMateria";
    static final String $TAREA_FECHA = "tarea_fecha";
    static final String $TAREA_HASFOTOS = "tarea_hasfotos";

    static final String CREAR_TABLA_TAREA = "CREATE TABLE " +
            TABLA_TAREA + " (" + $TAREA_ID + " " + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            $TAREA_DESC + " TEXT," + $TAREA_FECHA + " TEXT," + $TAREA_IDMATERIA + " INTEGER,"
            + $TAREA_HASFOTOS + " INTEGER)";

    public static void registrar_tarea(String date, String Desc, String idMateria, Context context) {
        ConexionSQLiteHelper conn = new_SQlite_Conection(context);
        SQLiteDatabase db = conn.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put($TAREA_DESC, Desc);
        values.put($TAREA_FECHA, date);
        values.put($TAREA_IDMATERIA, idMateria);

        db.insert(TABLA_TAREA, $TAREA_ID, values);
        Toast.makeText(context, "Se agrego la tarea uwu", Toast.LENGTH_SHORT).show();
        db.close();
    }
}