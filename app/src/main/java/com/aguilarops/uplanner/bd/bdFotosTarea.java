package com.aguilarops.uplanner.bd;

public class bdFotosTarea {
    static final String TABLA_PHTAREA = "phtarea";

    static final String $PHTAREA_ID = "phtarea_id";
    static final String $PHTAREA_PATH = "phtarea_desc";
    static final String $PHTAREA_IDTAREA = "phtarea_idTarea";

    static final String CREAR_TABLA_PHTAREA = "CREATE TABLE " +
            TABLA_PHTAREA + " (" + $PHTAREA_ID + " " + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            $PHTAREA_PATH + " TEXT," + $PHTAREA_IDTAREA + " INTEGER)";
}
