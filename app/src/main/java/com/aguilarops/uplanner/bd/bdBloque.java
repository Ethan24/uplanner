package com.aguilarops.uplanner.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aguilarops.uplanner.abstraction.appdata.materias.Bloque;
import com.aguilarops.uplanner.abstraction.appdata.materias.Materia;
import com.aguilarops.uplanner.util.Tiempo;

import java.util.ArrayList;

import static com.aguilarops.uplanner.bd.bdMateria.$MATERIA_COLOR;
import static com.aguilarops.uplanner.bd.bdMateria.$MATERIA_ID;
import static com.aguilarops.uplanner.bd.bdMateria.$MATERIA_NOMBRE;
import static com.aguilarops.uplanner.bd.bdMateria.TABLA_MATERIA;
import static com.aguilarops.uplanner.bd.bdUtil.getLEFTJOIN_clause;
import static com.aguilarops.uplanner.bd.bdUtil.new_SQlite_Conection;

public class bdBloque {
    static final String TABLA_BLOQUE = "bloque";

    static final String $BLOQUE_ID = "bloque_id";
    static final String $BLOQUE_HORA_INICIO = "bloque_hora_inicio";
    static final String $BLOQUE_MINUTO_INICIO = "bloque_minuto_inicio";
    static final String $BLOQUE_HORA_FIN = "bloque_hora_fin";
    static final String $BLOQUE_MINUTO_FIN = "bloque_minuto_fin";
    static final String $BLOQUE_DIA = "bloque_dia";
    static final String $BLOQUE_EDIFICIO = "bloque_edificio";
    static final String $BLOQUE_SECCION = "bloque_seccion";
    static final String $BLOQUE_IDMATERIA = "bloque_idMateria";

    static final String CREAR_TABLA_BLOQUE = "CREATE TABLE " +
            "" + TABLA_BLOQUE + " (" + $BLOQUE_ID + " " + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            $BLOQUE_HORA_INICIO + " INTEGER, " + $BLOQUE_MINUTO_INICIO + " INTEGER, " +
            $BLOQUE_HORA_FIN + " INTEGER, " + $BLOQUE_MINUTO_FIN + " INTEGER, " +
            $BLOQUE_DIA + " INTEGER, " + $BLOQUE_IDMATERIA + " INTEGER, "
            + $BLOQUE_EDIFICIO + " TEXT," + $BLOQUE_SECCION + " TEXT)";

    public static void registrar_empty_bloque(Context context, Tiempo inicio, Tiempo fin, int day) {
        ConexionSQLiteHelper conn = new_SQlite_Conection(context);
        SQLiteDatabase db = conn.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put($BLOQUE_HORA_INICIO, inicio.horas);
        values.put($BLOQUE_MINUTO_INICIO, inicio.minutos);
        values.put($BLOQUE_HORA_FIN, fin.horas);
        values.put($BLOQUE_MINUTO_FIN, fin.minutos);
        values.put($BLOQUE_DIA, day);

        db.insert(TABLA_BLOQUE, $BLOQUE_ID, values);
        //Toast.makeText(bd_context, "Id Registro: " + idResultante, Toast.LENGTH_SHORT).show();
        db.close();
    }

    public static void registrar_bloque(Context context, Tiempo inicio, Tiempo fin, String edifico, String seccion, String idMateria, int day) {
        ConexionSQLiteHelper conn = new_SQlite_Conection(context);
        SQLiteDatabase db = conn.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put($BLOQUE_HORA_INICIO, inicio.horas);
        values.put($BLOQUE_MINUTO_INICIO, inicio.minutos);
        values.put($BLOQUE_HORA_FIN, fin.horas);
        values.put($BLOQUE_MINUTO_FIN, fin.minutos);
        values.put($BLOQUE_EDIFICIO, edifico);
        values.put($BLOQUE_SECCION, seccion);
        values.put($BLOQUE_DIA, day);
        values.put($BLOQUE_IDMATERIA, idMateria);


        db.insert(TABLA_BLOQUE, $BLOQUE_ID, values);
//        System.out.println("bloque #: " + idResultante);
//        System.out.println("Dia: " + idDia);
        //Toast.makeText(bd_context, "Id Registro: " + idResultante, Toast.LENGTH_SHORT).show();
        db.close();
    }

    public static ArrayList<Bloque> getbloques_perDay2home(Context c, int day) {
        ConexionSQLiteHelper conn = new_SQlite_Conection(c);
        SQLiteDatabase db = conn.getReadableDatabase();

        Materia materia;
        Bloque bloque;

        ArrayList<Bloque> list = new ArrayList<>();

        String tables = TABLA_BLOQUE + " , " + TABLA_MATERIA;

        String[] campos = {$BLOQUE_EDIFICIO, $BLOQUE_SECCION, $BLOQUE_HORA_INICIO, $BLOQUE_MINUTO_INICIO,
                $BLOQUE_HORA_FIN, $BLOQUE_MINUTO_FIN,
                $MATERIA_NOMBRE, $MATERIA_COLOR};

        String where = $BLOQUE_IDMATERIA + " = " + $MATERIA_ID + " AND " +
                $BLOQUE_DIA + " = " + day;

        try {
            Cursor cursor = db.query(tables, campos, where, null, null, null, null);
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    materia = new Materia();
                    bloque = new Bloque();

                    bloque.edificio = cursor.getString(0);
                    bloque.seccion = cursor.getString(1);
                    bloque.inicio = new Tiempo(cursor.getInt(2), cursor.getInt(3));
                    bloque.fin = new Tiempo(cursor.getInt(4), cursor.getInt(5));
                    materia.name = cursor.getString(6);
                    materia.color = cursor.getInt(7);
                    bloque.materia = materia;

                    list.add(bloque);
                    cursor.moveToNext();
                }
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        db.close();
        return list;
    }

    public static ArrayList<Bloque> getHorario(Context c) {
        ConexionSQLiteHelper conn = new_SQlite_Conection(c);
        SQLiteDatabase db = conn.getReadableDatabase();
        ArrayList<Bloque> bloques = new ArrayList<>();
        Bloque bloque;
        Materia materia;
        String condition_of_join = $BLOQUE_IDMATERIA + " = " + $MATERIA_ID;
        String leftjoint_clause = getLEFTJOIN_clause(TABLA_BLOQUE, TABLA_MATERIA, condition_of_join);
        String[] campos = {$BLOQUE_ID, $BLOQUE_SECCION, $BLOQUE_HORA_INICIO,
                $BLOQUE_MINUTO_INICIO, $BLOQUE_HORA_FIN, $BLOQUE_MINUTO_FIN, $BLOQUE_DIA,
                $MATERIA_ID, $MATERIA_NOMBRE, $MATERIA_COLOR};

        try {
            Cursor cursor = db.query(leftjoint_clause, campos, null, null, null, null, $BLOQUE_ID);

            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    bloque = new Bloque();
                    materia = new Materia();

                    bloque.id = cursor.getString(0);
                    bloque.seccion = cursor.getString(1);
                    bloque.inicio = new Tiempo(cursor.getInt(2), cursor.getInt(3));
                    bloque.fin = new Tiempo(cursor.getInt(4), cursor.getInt(5));
                    bloque.day = cursor.getInt(6);
                    materia.id = cursor.getString(7);
                    materia.name = cursor.getString(8);
                    materia.color = cursor.getInt(9);
                    bloque.materia = materia;

                    bloques.add(bloque);

                    cursor.moveToNext();
                }
            }

            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return bloques;
    }

    public static ArrayList<Integer> getDays(Context c) {
        SQLiteDatabase db = new_SQlite_Conection(c).getReadableDatabase();
        String tabla1 = "tabla1"; //se referencia la misma tabla con dos nombres
        String tabla2 = "tabla2";   //haremos un left join asi para no tomar bloques que compartan la misma materia
        //aunque puedan tener diferentes secciones o edificios
        String condition_of_join = tabla1 + "." + $BLOQUE_DIA + " = " + tabla2 + "." + $BLOQUE_DIA + " AND "
                + tabla1 + "." + $BLOQUE_ID + " > " + tabla2 + "." + $BLOQUE_ID;

        String leftjoint_clause = getLEFTJOIN_clause(TABLA_BLOQUE
                + " " + tabla1, TABLA_BLOQUE + " " + tabla2, condition_of_join);

        String where_clause = tabla2 + "." + $BLOQUE_IDMATERIA + " IS NULL";

        String[] campos = {tabla1 + "." + $BLOQUE_DIA};

        ArrayList<Integer> list = new ArrayList<>();

        try {
            Cursor cursor = db.query(leftjoint_clause, campos, where_clause, null, null, null, tabla1 + "." + $BLOQUE_DIA);
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    list.add(cursor.getInt(0));
//                    System.out.println(list.get(list.size()-1));
                    cursor.moveToNext();
                }
            }

            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static Bloque get_limitHour(Context c) {
        SQLiteDatabase db = new_SQlite_Conection(c).getReadableDatabase();

        Bloque bloque = null;

        String[] campos = {"MIN(" + $BLOQUE_HORA_INICIO + ")", "MAX(" + $BLOQUE_HORA_FIN + ")"};


        try {
            Cursor cursor = db.query(TABLA_BLOQUE, campos, null, null, null, null, null);
            if (cursor.moveToFirst()) {
                bloque = new Bloque();

                bloque.inicio = new Tiempo(cursor.getInt(0) - (cursor.getInt(0) > 0 ? 1 : 0), 0);
                bloque.fin = new Tiempo(cursor.getInt(1) + (cursor.getInt(1) < 24 ? 1 : 0), 0);
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        db.close();
        return bloque;
    }
}
