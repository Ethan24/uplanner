package com.aguilarops.uplanner.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aguilarops.uplanner.abstraction.appdata.materias.Bloque;
import com.aguilarops.uplanner.abstraction.appdata.materias.Materia;
import com.aguilarops.uplanner.abstraction.appdata.materias.Profesor;
import com.aguilarops.uplanner.util.Util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

import static com.aguilarops.uplanner.bd.bdBloque.$BLOQUE_DIA;
import static com.aguilarops.uplanner.bd.bdBloque.$BLOQUE_EDIFICIO;
import static com.aguilarops.uplanner.bd.bdBloque.$BLOQUE_HORA_INICIO;
import static com.aguilarops.uplanner.bd.bdBloque.$BLOQUE_ID;
import static com.aguilarops.uplanner.bd.bdBloque.$BLOQUE_IDMATERIA;
import static com.aguilarops.uplanner.bd.bdBloque.$BLOQUE_MINUTO_INICIO;
import static com.aguilarops.uplanner.bd.bdBloque.$BLOQUE_SECCION;
import static com.aguilarops.uplanner.bd.bdBloque.TABLA_BLOQUE;
import static com.aguilarops.uplanner.bd.bdProfesor.$PROFE_ID;
import static com.aguilarops.uplanner.bd.bdProfesor.$PROFE_NOMBRE;
import static com.aguilarops.uplanner.bd.bdProfesor.TABLA_PROFESOR;
import static com.aguilarops.uplanner.bd.bdUtil.existBYname;
import static com.aguilarops.uplanner.bd.bdUtil.getID;
import static com.aguilarops.uplanner.bd.bdUtil.getLEFTJOIN_clause;
import static com.aguilarops.uplanner.bd.bdUtil.new_SQlite_Conection;

public class bdMateria {
    static final String TABLA_MATERIA = "materia";

    static final String $MATERIA_ID = "materia_id";
    static final String $MATERIA_NOMBRE = "materia_name";
    static final String $MATERIA_COLOR = "materia_color";
    static final String $MATERIA_GRUPO = "materia_grupo";
    static final String $MATERIA_IDPROFESOR = "materia_idProfesor";

    static final String CREAR_TABLA_MATERIA = "CREATE TABLE " +
            "" + TABLA_MATERIA + " (" + $MATERIA_ID + " " + "INTEGER PRIMARY KEY AUTOINCREMENT, " +
            $MATERIA_NOMBRE + " TEXT," + $MATERIA_COLOR + " INTEGER, " +
            $MATERIA_GRUPO + " TEXT, " + $MATERIA_IDPROFESOR + " INTEGER)";

    public static void registrar_materia(Context context, String name, String idProfe, String grupo) {
        ConexionSQLiteHelper conn = new_SQlite_Conection(context);
        SQLiteDatabase db = conn.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put($MATERIA_NOMBRE, name);
        values.put($MATERIA_GRUPO, grupo);
        values.put($MATERIA_IDPROFESOR, Integer.parseInt(idProfe));

        db.insert(TABLA_MATERIA, $MATERIA_ID, values);
        //Toast.makeText(bd_context, "Id Registro: " + idResultante, Toast.LENGTH_SHORT).show();
        db.close();
    }

    public static ArrayList<Bloque> getAsignaturasLoaded(Context context) {
        ConexionSQLiteHelper conn = new_SQlite_Conection(context);
        SQLiteDatabase db = conn.getReadableDatabase();
        ArrayList<Bloque> bloques = new ArrayList<>();
        Bloque bloque;
        Materia materia;
        Profesor profesor;

        String tabla1 = "tabla1"; //se referencia la misma tabla con dos nombres
        String tabla2 = "tabla2";   //haremos un left join asi para no tomar bloques que compartan la misma materia
        //aunque puedan tener diferentes secciones o edificios
        String condition_of_join = tabla1 + "." + $BLOQUE_IDMATERIA + " = " + tabla2 + "." + $BLOQUE_IDMATERIA + " AND "
                + tabla1 + "." + $BLOQUE_ID + " > " + tabla2 + "." + $BLOQUE_ID;

        String leftjoint_clause = TABLA_PROFESOR + " ," + TABLA_MATERIA + " ," + getLEFTJOIN_clause(TABLA_BLOQUE
                + " " + tabla1, TABLA_BLOQUE + " " + tabla2, condition_of_join);

        String where_clause = tabla2 + "." + $BLOQUE_IDMATERIA + " IS NULL" + " AND " +
                tabla1 + "." + $BLOQUE_IDMATERIA + " = " + $MATERIA_ID + " AND " +
                $PROFE_ID + " = " + $MATERIA_IDPROFESOR;

        String[] campos = {tabla1 + "." + $BLOQUE_EDIFICIO, tabla1 + "." + $BLOQUE_SECCION, $MATERIA_ID, $MATERIA_NOMBRE, $MATERIA_GRUPO,
                $PROFE_ID, $PROFE_NOMBRE};

        try {
            Cursor cursor = db.query(leftjoint_clause, campos, where_clause, null, null, null, tabla1 + "." + $BLOQUE_ID);

            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    bloque = new Bloque();
                    materia = new Materia();
                    profesor = new Profesor();

                    bloque.edificio = cursor.getString(0);
                    bloque.seccion = cursor.getString(1);
                    materia.id = cursor.getString(2);
                    materia.name = cursor.getString(3);
                    materia.grupo = cursor.getString(4);
//                    System.out.println(materia.name);
                    materia.dias = getMateria_days(db, materia.id);
//                    System.out.println(materia.name);
//                    for (int i = 0; i < materia.dias.size(); i++) {
//                        System.out.println(materia.dias.get(i));
//                    }
                    profesor.id = cursor.getString(5);
                    profesor.name = cursor.getString(6);
                    materia.profesor = profesor;
                    bloque.materia = materia;

                    bloques.add(bloque);

                    cursor.moveToNext();
                }
            }

            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return bloques;
    }

    static ArrayList<Integer> getMateria_days(SQLiteDatabase db, String idMateria) {
        String tabla1 = "tabla1"; //se referencia la misma tabla con dos nombres
        String tabla2 = "tabla2";   //haremos un left join asi para no tomar bloques que compartan la misma materia
        //aunque puedan tener diferentes secciones o edificios
        String condition_of_join = tabla1 + "." + $BLOQUE_DIA + " = " + tabla2 + "." + $BLOQUE_DIA + " AND "
                + tabla1 + "." + $BLOQUE_IDMATERIA + " = " + tabla2 + "." + $BLOQUE_IDMATERIA + " AND "
                + tabla1 + "." + $BLOQUE_ID + " > " + tabla2 + "." + $BLOQUE_ID;

        String leftjoint_clause = getLEFTJOIN_clause(TABLA_BLOQUE
                + " " + tabla1, TABLA_BLOQUE + " " + tabla2, condition_of_join);

        String where_clause = tabla2 + "." + $BLOQUE_IDMATERIA + " IS NULL" + " AND " +
                tabla1 + "." + $BLOQUE_IDMATERIA + " = ?";

        String[] campos = {tabla1 + "." + $BLOQUE_DIA};

        String[] parametros = {idMateria};
        ArrayList<Integer> list = new ArrayList<>();

        try {
            Cursor cursor = db.query(leftjoint_clause, campos, where_clause, parametros, null, null, tabla1 + "." + $BLOQUE_DIA);
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    list.add(cursor.getInt(0));
//                    System.out.println(list.get(list.size()-1));
                    cursor.moveToNext();
                }
            }

            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }


    static ArrayList<Integer> getALl_Materia_days(SQLiteDatabase db, String idMateria) {
        String where_clause = $BLOQUE_IDMATERIA + " = ?";

        String[] campos = {$BLOQUE_DIA};

        String[] parametros = {idMateria};
        ArrayList<Integer> list = new ArrayList<>();

        try {
            Cursor cursor = db.query(TABLA_BLOQUE, campos, where_clause, parametros, null, null, $BLOQUE_DIA);
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    list.add(cursor.getInt(0));
//                    System.out.println(list.get(list.size()-1));
                    cursor.moveToNext();
                }
            }

            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static ArrayList<Integer> getMateria_days(Context c, String idMateria) {
        return getMateria_days(new_SQlite_Conection(c).getReadableDatabase(), idMateria);
    }

    public static void materia_set_color(Context c, String idMateria, Integer color) {
        SQLiteDatabase db = new_SQlite_Conection(c).getWritableDatabase();

        String[] parametros = {idMateria};
        ContentValues values = new ContentValues();
        values.put($MATERIA_COLOR, color);

        db.update(TABLA_MATERIA, values, $MATERIA_ID + "=?", parametros);
//        Toast.makeText(c,"Ya se actualizó el usuario",Toast.LENGTH_LONG).show();
        db.close();
    }

    public static boolean materia_exist(Context c, String name) {
        return existBYname(c, $MATERIA_NOMBRE, name, TABLA_MATERIA);
    }

    public static String getMateria_id(Context c, String name) {
        return getID(c, TABLA_MATERIA, name, $MATERIA_NOMBRE, $MATERIA_ID);
    }

    private static String getnext(SQLiteDatabase db, int now, int nextDay, String idMateria, Integer[] days, int idx) {
        String where_clause = $BLOQUE_IDMATERIA + " = ?" + " AND " +
                $BLOQUE_DIA + " = ?";

        String[] campos = {$BLOQUE_HORA_INICIO, $BLOQUE_MINUTO_INICIO};
        String orderBy = $BLOQUE_HORA_INICIO + " AND " + $BLOQUE_MINUTO_INICIO;
        String[] parametros = new String[]{idMateria, String.valueOf(nextDay)};

        String hora = "";
        int nextValue = -1;
        try {
            Cursor cursor = db.query(TABLA_BLOQUE, campos, where_clause, parametros, null, null, orderBy);

            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    int iHora = cursor.getInt(0);
                    int iMinute = cursor.getInt(1);
                    if (nextDay == now) {
                        nextValue = Util.getWeight_hour(iHora, iMinute);
                        if (Util.getNowTimeWeight() <
                                nextValue) {
                            System.out.println("dah");
                            hora = Util.getHoraFormat_from24(iHora, iMinute);
                            break;
                        } else {
                            cursor.moveToNext();
                        }
                    } else {
                        hora = Util.getHoraFormat_from24(iHora, iMinute);
                        break;
                    }
                }
            }

            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (nextDay == now) {
            if (Util.getNowTimeWeight() >= nextValue && nextValue >= 0) {
                if (idx < (days.length - 1)) {
                    nextDay = days[++idx];
                } else {
                    idx = 0;
                    nextDay = days[idx];
                }
                return getnext(db, now, nextDay, idMateria, days, idx);
            }
        }
        return nextDay + "," + hora;
    }

    static String nextClass(SQLiteDatabase db, String idMateria) {
        Calendar cal = Calendar.getInstance();
        int now = cal.get(Calendar.DAY_OF_WEEK) - 1;
        System.out.println("now " + now);
        Integer[] days = getALl_Materia_days(db, idMateria).toArray(new Integer[0]);

        //Vemos que dia de la semana(en los que se recibe la materia)
//        Esta mas cercano al dia de hoy(en la semana)
        int distance = Math.abs(days[0] - now);
        System.out.println(Arrays.toString(days));
        int idx = 0;

        for (int i = 1; i < days.length; i++) {
            int idistance = Math.abs(days[i] - now);
            if (idistance < distance) {
                idx = i;
                distance = idistance;
            }
        }

        int c = days[idx];//c es ese dia mas cercano
        if (c < now) {
            if (idx < (days.length - 1)) {
                c = days[++idx];
            } else {
                idx = 0;
                c = days[idx];
            }
        }
        String[] result = getnext(db, now, c, idMateria, days, idx).split(",");

        StringBuilder retorn = new StringBuilder();
        int day = Integer.parseInt(result[0]);
        System.out.println("la " + day);
        if (day == now) {
            retorn.append("Hoy");
        } else {
            cal.set(Calendar.DAY_OF_WEEK, day + 1);
            SimpleDateFormat sdf = new SimpleDateFormat("EEE", Locale.getDefault());
            char[] sMyDate = sdf.format(cal.getTime()).toCharArray();
            sMyDate[0] = Character.toUpperCase(sMyDate[0]);
            for (char l : sMyDate
            ) {
                retorn.append(l);
            }
        }

        return retorn.append(", ").append(result[1]).toString();
    }

    public static ArrayList<Materia> getMaterias(Context context) {
        SQLiteDatabase db = new_SQlite_Conection(context).getReadableDatabase();
        ArrayList<Materia> materias = new ArrayList<>();
        Materia materia;

        String[] campos = {$MATERIA_ID, $MATERIA_NOMBRE, $MATERIA_GRUPO, $MATERIA_COLOR};

        try {
            Cursor cursor = db.query(TABLA_MATERIA, campos, null, null, null, null, $MATERIA_ID);

            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    materia = new Materia();

                    materia.id = cursor.getString(0);
                    materia.name = cursor.getString(1);
                    materia.grupo = cursor.getString(2);
                    materia.color = cursor.getInt(3);
                    materia.nextClass = nextClass(db, materia.id);

                    materias.add(materia);

                    cursor.moveToNext();
                }
            }

            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return materias;
    }

    public static ArrayList<Materia> getList_of_Materias2Select(Context context) {
        SQLiteDatabase db = new_SQlite_Conection(context).getReadableDatabase();
        ArrayList<Materia> materias = new ArrayList<>();
        Materia materia;

        String[] campos = {$MATERIA_ID, $MATERIA_NOMBRE, $MATERIA_COLOR};

        try {
            Cursor cursor = db.query(TABLA_MATERIA, campos, null, null, null, null, $MATERIA_ID);

            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    materia = new Materia();

                    materia.id = cursor.getString(0);
                    materia.name = cursor.getString(1);
                    materia.color = cursor.getInt(2);

                    materias.add(materia);

                    cursor.moveToNext();
                }
            }

            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return materias;
    }
}
