package com.aguilarops.uplanner.bd;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class ConexionSQLiteHelper extends SQLiteOpenHelper {

    public ConexionSQLiteHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    //    Creamos los scrips
    @Override
    public void onCreate(SQLiteDatabase db) {
        bdUtil.CREATE_TABLES(db);
//        db.execSQL(Bd_Util.CREAR_TABLA_USER);
//        db.execSQL(Bd_Util.CREAR_TABLA_PROFESOR);
//        db.execSQL(Bd_Util.CREAR_TABLA_MATERIA);
//        db.execSQL(Bd_Util.CREAR_TABLA_TAREA);
//        db.execSQL(Bd_Util.CREAR_TABLA_BLOQUE);
    }

    //    Refrescamos los scripts
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        bdUtil.DROP_TABLES(db);
//        Bd_Util.dropTable(db, Bd_Util.TABLA_USER);
//        Bd_Util.dropTable(db, Bd_Util.TABLA_PROFESOR);
//        Bd_Util.dropTable(db, Bd_Util.TABLA_MATERIA);
//        Bd_Util.dropTable(db, Bd_Util.TABLA_TAREA);
//        Bd_Util.dropTable(db, Bd_Util.TABLA_BLOQUE);
        onCreate(db);
    }
}

