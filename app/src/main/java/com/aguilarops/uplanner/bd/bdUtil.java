package com.aguilarops.uplanner.bd;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import static com.aguilarops.uplanner.bd.bdBloque.CREAR_TABLA_BLOQUE;
import static com.aguilarops.uplanner.bd.bdBloque.TABLA_BLOQUE;
import static com.aguilarops.uplanner.bd.bdFotosTarea.CREAR_TABLA_PHTAREA;
import static com.aguilarops.uplanner.bd.bdFotosTarea.TABLA_PHTAREA;
import static com.aguilarops.uplanner.bd.bdMateria.CREAR_TABLA_MATERIA;
import static com.aguilarops.uplanner.bd.bdMateria.TABLA_MATERIA;
import static com.aguilarops.uplanner.bd.bdProfesor.CREAR_TABLA_PROFESOR;
import static com.aguilarops.uplanner.bd.bdProfesor.TABLA_PROFESOR;
import static com.aguilarops.uplanner.bd.bdTarea.CREAR_TABLA_TAREA;
import static com.aguilarops.uplanner.bd.bdTarea.TABLA_TAREA;
import static com.aguilarops.uplanner.bd.bdUser.$USER_NOMBRE;
import static com.aguilarops.uplanner.bd.bdUser.CREAR_TABLA_USER;
import static com.aguilarops.uplanner.bd.bdUser.TABLA_USER;

public class bdUtil {
    public static final String $BD_NAME = "base_de_data";
    public static final String $SP_NAME = "credenciales";
    public static final int $BD_VERSION = 1;

    public static ConexionSQLiteHelper new_SQlite_Conection(Context context) {
        return new ConexionSQLiteHelper(context, $BD_NAME, null, $BD_VERSION);
    }

    static void CREATE_TABLES(SQLiteDatabase db) {
        db.execSQL(CREAR_TABLA_USER);
        db.execSQL(CREAR_TABLA_PROFESOR);
        db.execSQL(CREAR_TABLA_MATERIA);
        db.execSQL(CREAR_TABLA_BLOQUE);
        db.execSQL(CREAR_TABLA_TAREA);
        db.execSQL(CREAR_TABLA_PHTAREA);
    }

    private static String Command_dropTable(String table) {
        return "DROP TABLE IF EXISTS " + table;
    }

    private static void dropTable(SQLiteDatabase db, String table) {
        db.execSQL(Command_dropTable(table));
    }

    static void DROP_TABLES(SQLiteDatabase db) {
        dropTable(db, TABLA_USER);
        dropTable(db, TABLA_PROFESOR);
        dropTable(db, TABLA_MATERIA);
        dropTable(db, TABLA_BLOQUE);
        dropTable(db, TABLA_TAREA);
        dropTable(db, TABLA_PHTAREA);
    }


    public static String getLog(Context context) {
        SharedPreferences preferences = context.getSharedPreferences($SP_NAME, Context.MODE_PRIVATE);

        return preferences.getString($USER_NOMBRE, null);
    }

    public static boolean isThereLogged(Context context) {
        SharedPreferences preferences = context.getSharedPreferences($SP_NAME, Context.MODE_PRIVATE);

        return preferences.getString($USER_NOMBRE, null) != null;
    }

    public static void clearPreferences(Context context) {
        SharedPreferences preferences = context.getSharedPreferences($SP_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.clear();
        editor.apply();
    }

    public static void setLog(String nombre, Context context) {
        SharedPreferences preferences = context.getSharedPreferences($SP_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString($USER_NOMBRE, nombre);
        editor.apply();
    }


    static String getLEFTJOIN_clause(String table, String other_table, String condition) {
        return table + " LEFT OUTER JOIN " + other_table + " ON " + condition;
    }

    static boolean existBYname(Context context, String campo_nombre, String name, String tabla) {
        ConexionSQLiteHelper conn = new_SQlite_Conection(context);
        SQLiteDatabase db = conn.getReadableDatabase();
        ArrayList<String> parametros = new ArrayList<>();
        parametros.add(name);
        String Selection;

        Selection = campo_nombre + "=?";

        try {
            Cursor cursor = db.query(tabla, null, Selection, parametros.toArray(new String[]{}), null, null, null);
            cursor.moveToFirst();
            int count = cursor.getCount();
            cursor.close();
            return count > 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        db.close();
        return false;
    }

    static String getID(Context c, String tabla, String name, String campo_nombre, String campoid) {
        ConexionSQLiteHelper conn = new_SQlite_Conection(c);
        SQLiteDatabase db = conn.getReadableDatabase();
        ArrayList<String> parametros = new ArrayList<>();
        String[] campos = {campoid};
        parametros.add(name);
        String Selection;
        String id = null;
        Selection = campo_nombre + " = ?";

        try {
            Cursor cursor = db.query(tabla, campos, Selection, parametros.toArray(new String[]{}), null, null, null);
            cursor.moveToFirst();
            id = cursor.getCount() > 0 ? cursor.getString(0) : null;
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        db.close();
        return id;
    }
}
