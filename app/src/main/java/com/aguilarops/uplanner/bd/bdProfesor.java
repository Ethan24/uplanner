package com.aguilarops.uplanner.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Patterns;
import android.widget.Toast;

import com.aguilarops.uplanner.abstraction.appdata.materias.Materia;
import com.aguilarops.uplanner.abstraction.appdata.materias.Profesor;
import com.aguilarops.uplanner.util.Util;

import java.util.ArrayList;

import static com.aguilarops.uplanner.bd.bdMateria.$MATERIA_COLOR;
import static com.aguilarops.uplanner.bd.bdMateria.$MATERIA_ID;
import static com.aguilarops.uplanner.bd.bdMateria.$MATERIA_IDPROFESOR;
import static com.aguilarops.uplanner.bd.bdMateria.$MATERIA_NOMBRE;
import static com.aguilarops.uplanner.bd.bdMateria.TABLA_MATERIA;
import static com.aguilarops.uplanner.bd.bdUtil.existBYname;
import static com.aguilarops.uplanner.bd.bdUtil.getID;
import static com.aguilarops.uplanner.bd.bdUtil.getLEFTJOIN_clause;
import static com.aguilarops.uplanner.bd.bdUtil.new_SQlite_Conection;

public class bdProfesor {
    static final String TABLA_PROFESOR = "profesor";

    static final String $PROFE_ID = "profe_id";
    static final String $PROFE_NOMBRE = "profe_name";
    static final String $PROFE_TELEFONO = "profe_telefono";
    static final String $PROFE_CORREO = "profe_correo";

    static final String CREAR_TABLA_PROFESOR = "CREATE TABLE " +
            "" + TABLA_PROFESOR + " (" + $PROFE_ID + " " + "INTEGER PRIMARY KEY AUTOINCREMENT, " +
            $PROFE_NOMBRE + " TEXT," + $PROFE_TELEFONO + " TEXT," + $PROFE_CORREO + " TEXT)";

    public static void registrar_profesor(Context c, String name) {
        ConexionSQLiteHelper conn = new_SQlite_Conection(c);
        SQLiteDatabase db = conn.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put($PROFE_NOMBRE, name);
        db.insert(TABLA_PROFESOR, $PROFE_ID, values);
        //Toast.makeText(bd_c, "Id Registro: " + idResultante, Toast.LENGTH_SHORT).show();
        db.close();
    }

    public static void registrar_profesor(Context c, String name, String telef, String email) {
        ConexionSQLiteHelper conn = new_SQlite_Conection(c);
        SQLiteDatabase db = conn.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put($PROFE_NOMBRE, name);
        boolean containsArea = false;
        boolean realPhone = false;
        boolean realEmail = false;
        assert telef != null;
        if (!telef.isEmpty()) {
            if (telef.contains("+")) {
                if (telef.indexOf("+") == telef.lastIndexOf("+")) {
                    containsArea = true;
                    telef = telef.replace("+", "");
                }
            }

            int length = 8;
            if (telef.contains(" ")) {
                length += telef.indexOf(" ") + 1;
            }

            if (Util.isNumeric(telef.replace(" ", "")) && telef.length() >= length) {
                realPhone = true;
            }

            if (!containsArea) {
                telef = "+505 " + telef;
            } else {
                telef = "+" + telef;
            }
        }
        if (realPhone || telef.isEmpty()) values.put($PROFE_TELEFONO, telef);
        if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            realEmail = true;
        }
        if (realEmail || email.isEmpty()) values.put($PROFE_CORREO, email);
        String message;

        if (!realPhone && !realEmail && !telef.isEmpty() && !email.isEmpty()) {
            message = "Numero y Correo invalidos";
        } else if (!realPhone && !telef.isEmpty()) {
            message = "Numero de Telefono invalido";
        } else if (!realEmail && !email.isEmpty()) {
            message = "Correo invalido";
        } else {
            message = "Registro de contacto exitoso";
        }

        db.insert(TABLA_PROFESOR, $PROFE_ID, values);
        Toast.makeText(c, message, Toast.LENGTH_LONG).show();
        //Toast.makeText(bd_c, "Id Registro: " + idResultante, Toast.LENGTH_SHORT).show();
        db.close();
    }

    public static void setProfesor_contact(Context c, String telef, String email, String idProfe) {
        if (idProfe == null) return;
        ConexionSQLiteHelper conn = new_SQlite_Conection(c);
        SQLiteDatabase db = conn.getWritableDatabase();
        String selection = $PROFE_ID + " = ?";

        String[] parametros = {idProfe};
        ContentValues values = new ContentValues();
        values.put($PROFE_TELEFONO, telef);
        values.put($PROFE_CORREO, email);

        db.update(TABLA_PROFESOR, values, selection, parametros);
//            System.out.println(values.get($PROFE_TELEFONO));
//            System.out.println(values.get($PROFE_CORREO));

        db.close();
    }

    public static boolean profesor_exist(Context c, String name) {
        return existBYname(c, $PROFE_NOMBRE, name, TABLA_PROFESOR);
    }

    public static String getProfesor_id(Context c, String name) {
        return getID(c, TABLA_PROFESOR, name, $PROFE_NOMBRE, $PROFE_ID);
    }

    public static ArrayList<Materia> getProfesores2show(Context c) {
        ConexionSQLiteHelper conn = new_SQlite_Conection(c);
        SQLiteDatabase db = conn.getReadableDatabase();
        ArrayList<Materia> List = new ArrayList<>();
        Materia materia;
        Profesor profesor;

        String tabla1 = "tabla1"; //se referencia la misma tabla con dos nombres
        String tabla2 = "tabla2";   //haremos un left join asi para no tomar bloques que compartan la misma materia
        //aunque puedan tener diferentes secciones o edificios
        String condition_of_join = tabla1 + "." + $MATERIA_IDPROFESOR + " = " + tabla2 + "." + $MATERIA_IDPROFESOR + " AND "
                + tabla1 + "." + $MATERIA_ID + " > " + tabla2 + "." + $MATERIA_ID;

        String leftjoint_clause = TABLA_PROFESOR + " ," + getLEFTJOIN_clause(TABLA_MATERIA
                + " " + tabla1, TABLA_MATERIA + " " + tabla2, condition_of_join);

        String where_clause = tabla2 + "." + $MATERIA_IDPROFESOR + " IS NULL" + " AND " +
                $PROFE_ID + " = " + tabla1 + "." + $MATERIA_IDPROFESOR;

        String[] campos = {tabla1 + "." + $MATERIA_NOMBRE, tabla1 + "." + $MATERIA_COLOR,
                $PROFE_ID, $PROFE_NOMBRE, $PROFE_TELEFONO, $PROFE_CORREO};

        try {
            Cursor cursor = db.query(leftjoint_clause, campos, where_clause, null, null, null, tabla1 + "." + $MATERIA_ID);

            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    materia = new Materia();
                    profesor = new Profesor();

                    materia.name = cursor.getString(0);
                    materia.color = cursor.getInt(1);
                    profesor.id = cursor.getString(2);
                    profesor.name = cursor.getString(3);
                    profesor.telefono = cursor.getString(4);
                    profesor.correo = cursor.getString(5);
                    materia.profesor = profesor;

                    List.add(materia);

                    cursor.moveToNext();
                }
            }

            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return List;
    }

    public static ArrayList<Materia> getList_of_Docentes2Select(Context context) {
        SQLiteDatabase db = new_SQlite_Conection(context).getReadableDatabase();
        ArrayList<Materia> profesors = new ArrayList<>();
        Profesor docente;
        Materia materia;

        String condition_of_join = $MATERIA_IDPROFESOR + " = " + $PROFE_ID;

        String leftjoint_clause = getLEFTJOIN_clause(TABLA_PROFESOR
                , TABLA_MATERIA, condition_of_join);
        String[] campos = {$PROFE_ID, $PROFE_NOMBRE, $MATERIA_COLOR};

        try {
            Cursor cursor = db.query(leftjoint_clause, campos, null, null, null, null, $PROFE_ID);

            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    docente = new Profesor();
                    materia = new Materia();

                    docente.id = cursor.getString(0);
                    docente.name = cursor.getString(1);
                    materia.color = cursor.getInt(2);
                    materia.profesor = docente;

                    profesors.add(materia);

                    cursor.moveToNext();
                }
            }

            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return profesors;
    }
}
