package com.aguilarops.uplanner.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import java.util.ArrayList;

import static com.aguilarops.uplanner.bd.bdUtil.isThereLogged;
import static com.aguilarops.uplanner.bd.bdUtil.new_SQlite_Conection;
import static com.aguilarops.uplanner.bd.bdUtil.setLog;

public class bdUser {
    static final String TABLA_USER = "user";

    static final String $USER_NOMBRE = "user_name";

    static final String CREAR_TABLA_USER = "CREATE TABLE " +
            "" + TABLA_USER + " (" + $USER_NOMBRE + " TEXT)";


    public static void registrarUsuario(Context context, String name) {
        ConexionSQLiteHelper conn = new_SQlite_Conection(context);
        SQLiteDatabase db = conn.getWritableDatabase();

        if (!isThereLogged(context)) {
            ContentValues values = new ContentValues();
            values.put($USER_NOMBRE, name);
            db.insert(TABLA_USER, null, values);
            //Toast.makeText(bd_context, "Id Registro: " + idResultante, Toast.LENGTH_SHORT).show();
            setLog(name, context);
            db.close();
            return;
        } else {
            Toast.makeText(context, "Ya se ha registrado un usuario", Toast.LENGTH_SHORT).show();
        }
        db.close();
    }

    public int getUser_Horario_type(Context c) {
        ArrayList<Integer> days = bdBloque.getDays(c);
        boolean semanal = false;
        boolean finde = false;

        if (days.contains(0) || days.contains(6)) finde = true;

        for (int i = 1; i < 6; i++) {
            if (days.contains(i)) {
                semanal = true;
                break;
            }
        }

        if (semanal && finde) {
            return 2;
        } else if (semanal) {
            return 0;
        } else if (finde) {
            return 1;
        } else {
            return -1;
        }
    }
}
