package com.aguilarops.uplanner.util.filechooser.controller;

public interface NotifyItemChecked {
    void notifyCheckBoxIsClicked();
}