package com.aguilarops.uplanner.util.filechooser.model;


import android.os.Environment;

abstract class DialogConfigs {
    /*  PARENT_DIRECTORY*/
    private static final String DIRECTORY_SEPARATOR = "/";
    private static final String STORAGE_DIR = Environment.getExternalStorageDirectory().toString();

    /*  DEFAULT_DIR is the default mount point of the SDCARD. It is the default
     *  mount point.
     */
    static final String DEFAULT_DIR = DIRECTORY_SEPARATOR + STORAGE_DIR;
}