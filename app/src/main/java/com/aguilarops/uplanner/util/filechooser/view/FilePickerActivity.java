package com.aguilarops.uplanner.util.filechooser.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.aguilarops.uplanner.App.Activities.FillData.DataLoadedActivity;
import com.aguilarops.uplanner.App.ShortDialog;
import com.aguilarops.uplanner.PDF.PDFManager;
import com.aguilarops.uplanner.R;
import com.aguilarops.uplanner.abstraction.user.horario.Horario;
import com.aguilarops.uplanner.util.filechooser.controller.adapters.FileListAdapter;
import com.aguilarops.uplanner.util.filechooser.model.DialogProperties;
import com.aguilarops.uplanner.util.filechooser.model.FileListItem;
import com.aguilarops.uplanner.util.filechooser.model.MarkedItemList;
import com.aguilarops.uplanner.util.filechooser.utils.ExtensionFilter;
import com.aguilarops.uplanner.util.filechooser.utils.Utility;
import com.aguilarops.uplanner.util.filechooser.widget.MaterialCheckbox;
import com.airbnb.lottie.LottieAnimationView;
import com.tom_roush.pdfbox.util.PDFBoxResourceLoader;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FilePickerActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private static final int EXTERNAL_READ_PERMISSION_GRANT = 112;
    private ListView listView;
    private TextView dname, dir_path, title;
    private DialogProperties properties;
    private ArrayList<FileListItem> internalList;
    private ExtensionFilter filter;
    private FileListAdapter mFileListAdapter;
    private Button select;
    private String titleStr = null;
    private String positiveBtnNameStr = null;
    private String paths;
    Handler h;
    LoadDialog loadDialog;

    private void PDF_Manager_setup() {
        // Enable Android-style asset loading (highly recommended)
        PDFBoxResourceLoader.init(getApplicationContext());

        // Need to ask for write permissions on SDK 23 and up, this is ignored on older versions
        if (ContextCompat.checkSelfPermission(FilePickerActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(FilePickerActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

    @SuppressLint("HandlerLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (!Utility.checkStorageAccessPermissions(this)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission
                        .READ_EXTERNAL_STORAGE}, EXTERNAL_READ_PERMISSION_GRANT);
            }
        }

        h = new Handler() {
            @SuppressLint("SetTextI18n")
            public void handleMessage(@NonNull Message msg) {
                if (msg.what == 0) {
                    new ShortDialog(FilePickerActivity.this, "Error",
                            "Seleccione un Horario valido de la UNI").Show();
                }
                System.out.println(msg);
            }
        };
        PDF_Manager_setup();


        super.onCreate(savedInstanceState);

        properties = new DialogProperties();
        filter = new ExtensionFilter();
        internalList = new ArrayList<>();

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_main);
        listView = findViewById(R.id.fileList);
        select = findViewById(R.id.select);
        select.setEnabled((MarkedItemList.getSelectedItem() != null));

        dname = findViewById(R.id.dname);
        title = findViewById(R.id.title);
        dir_path = findViewById(R.id.dir_path);

        select.setOnClickListener(view -> {
            loadDialog = new LoadDialog(FilePickerActivity.this);
            loadDialog.setCancelable(false);
            loadDialog.setCanceledOnTouchOutside(false);
            paths = MarkedItemList.getSelectedItem();
//                Toast.makeText(FilePickerActivity.this, paths, Toast.LENGTH_LONG).show();
            loadDialog.show();
            new Thread(() -> {
                try {
                    PDFManager pdfManager = new PDFManager(getApplicationContext(), paths);
                    pdfManager.read_horario();
                    pdfManager.close();
                } catch (Exception ex) {
//                            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
                    Logger.getLogger(FilePickerActivity.class.getName()).log(Level.SEVERE, null, ex);
                }
            }).start();
        });

        mFileListAdapter = new FileListAdapter(internalList, this);
        mFileListAdapter.setNotifyItemCheckedListener(() -> {
            positiveBtnNameStr = positiveBtnNameStr == null ?
                    getResources().getString(R.string.choose_button_label) : positiveBtnNameStr;
            select.setEnabled((MarkedItemList.getSelectedItem() != null));
            mFileListAdapter.notifyDataSetChanged();
        });
        listView.setAdapter(mFileListAdapter);

        setTitle();
    }

    private void setTitle() {
        if (title == null || dname == null) {
            return;
        }
        if (titleStr != null) {
            if (title.getVisibility() == View.INVISIBLE) {
                title.setVisibility(View.VISIBLE);
            }
            title.setText(titleStr);
            if (dname.getVisibility() == View.VISIBLE) {
                dname.setVisibility(View.INVISIBLE);
            }
        } else {
            if (title.getVisibility() == View.VISIBLE) {
                title.setVisibility(View.INVISIBLE);
            }
            if (dname.getVisibility() == View.INVISIBLE) {
                dname.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        positiveBtnNameStr = (
                positiveBtnNameStr == null ?
                        getResources().getString(R.string.choose_button_label) :
                        positiveBtnNameStr
        );
        select.setText(positiveBtnNameStr);
        if (Utility.checkStorageAccessPermissions(this)) {
            File currLoc;
            internalList.clear();
            if (properties.offset.isDirectory() && validateOffsetPath()) {
                currLoc = new File(properties.offset.getAbsolutePath());
                FileListItem parent = new FileListItem();
                parent.setFilename(getString(R.string.label_parent_dir));
                parent.setDirectory(true);
                parent.setLocation(Objects.requireNonNull(currLoc.getParentFile())
                        .getAbsolutePath());
                parent.setTime(currLoc.lastModified());
                internalList.add(parent);
            } else if (properties.root.exists() && properties.root.isDirectory()) {
                currLoc = new File(properties.root.getAbsolutePath());
            } else {
                currLoc = new File(properties.error_dir.getAbsolutePath());
            }
            dname.setText(currLoc.getName());
            dir_path.setText(currLoc.getAbsolutePath());
            setTitle();
            internalList = Utility.prepareFileListEntries(internalList, currLoc, filter,
                    properties.show_hidden_files);
            mFileListAdapter.notifyDataSetChanged();
            listView.setOnItemClickListener(this);
        }
    }

    private boolean validateOffsetPath() {
        String offset_path = properties.offset.getAbsolutePath();
        String root_path = properties.root.getAbsolutePath();
        return !offset_path.equals(root_path) && offset_path.contains(root_path);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (internalList.size() > i) {
            FileListItem fitem = internalList.get(i);
            if (fitem.isDirectory()) {
                if (new File(fitem.getLocation()).canRead()) {
                    File currLoc = new File(fitem.getLocation());
                    dname.setText(currLoc.getName());
                    setTitle();
                    dir_path.setText(currLoc.getAbsolutePath());
                    internalList.clear();
                    if (!currLoc.getName().equals(properties.root.getName())) {
                        FileListItem parent = new FileListItem();
                        parent.setFilename(getString(R.string.label_parent_dir));
                        parent.setDirectory(true);
                        parent.setLocation(Objects.requireNonNull(currLoc
                                .getParentFile()).getAbsolutePath());
                        parent.setTime(currLoc.lastModified());
                        internalList.add(parent);
                    }
                    internalList = Utility.prepareFileListEntries(internalList, currLoc, filter,
                            properties.show_hidden_files);
                    mFileListAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(FilePickerActivity.this, "Error",
                            Toast.LENGTH_SHORT).show();
                }
            } else {

                MaterialCheckbox fmark = view.findViewById(R.id.file_mark);
                fmark.performClick();
            }
        }
    }


    @Override
    public void setTitle(CharSequence titleStr) {
        if (titleStr != null) {
            this.titleStr = titleStr.toString();
        } else {
            this.titleStr = null;
        }
        setTitle();
    }

    @Override
    public void onBackPressed() {
        //currentDirName is dependent on dname
        String currentDirName = dname.getText().toString();
        if (internalList.size() > 0) {
            FileListItem fitem = internalList.get(0);
            File currLoc = new File(fitem.getLocation());
            if (currentDirName.equals(properties.root.getName()) ||
                    !currLoc.canRead()) {
                super.onBackPressed();
            } else {
                dname.setText(currLoc.getName());
                dir_path.setText(currLoc.getAbsolutePath());
                internalList.clear();
                if (!currLoc.getName().equals(properties.root.getName())) {
                    FileListItem parent = new FileListItem();
                    parent.setFilename(getString(R.string.label_parent_dir));
                    parent.setDirectory(true);
                    parent.setLocation(Objects.requireNonNull(currLoc.getParentFile())
                            .getAbsolutePath());
                    parent.setTime(currLoc.lastModified());
                    internalList.add(parent);
                }
                internalList = Utility.prepareFileListEntries(internalList, currLoc, filter,
                        properties.show_hidden_files);
                mFileListAdapter.notifyDataSetChanged();
            }
            setTitle();
        } else {
            super.onBackPressed();
        }
    }

    class LoadDialog extends Dialog {

        private LottieAnimationView animationView;

        public LoadDialog(@NonNull Context context) {
            super(context);
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.loading_dialog);

            this.animationView = findViewById(R.id.loadAnim);
            animationView.playAnimation();

            new Thread(() -> {
                Looper.prepare();

                while (true) {
                    if (Horario.isFull()) {
                        break;
                    }
                }
                animationView.cancelAnimation();
                if (Horario.isSucces()) {
                    startActivity(new Intent(FilePickerActivity.this, DataLoadedActivity.class));
                } else {
                    h.sendEmptyMessage(0);
                    loadDialog.dismiss();
                    Horario.setVoid();
                }
            }).start();
        }

        @Override
        public void dismiss() {
            super.dismiss();
            animationView.cancelAnimation();
        }
    }
}

