package com.aguilarops.uplanner.util.filechooser.model;

import java.io.File;

public class DialogProperties {

    public File root;
    public File error_dir;
    public File offset;
    public boolean show_hidden_files;

    public DialogProperties() {
        root = new File(DialogConfigs.DEFAULT_DIR);
        error_dir = new File(DialogConfigs.DEFAULT_DIR);
        offset = new File(DialogConfigs.DEFAULT_DIR);
        show_hidden_files = false;
    }
}