package com.aguilarops.uplanner.util;

public class dia {
    public static int getValue(String inicio) {
        boolean isPM = false;
        if (inicio.toLowerCase().contains("pm")) isPM = true;
        inicio = inicio.toLowerCase().replace("am", "").replace("pm", "");
        String[] parts_of_hora = inicio.trim().split(":");
        int hora_first = Integer.parseInt(parts_of_hora[0]);

        if (isPM && hora_first < 12) hora_first += 12;

        return Integer.parseInt(hora_first + parts_of_hora[1]);
    }
}
