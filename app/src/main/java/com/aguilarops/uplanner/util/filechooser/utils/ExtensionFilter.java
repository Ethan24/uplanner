package com.aguilarops.uplanner.util.filechooser.utils;

import java.io.File;
import java.io.FileFilter;
import java.util.Locale;


public class ExtensionFilter implements FileFilter {

    public ExtensionFilter() {

    }

    @Override
    public boolean accept(File file) {
        if (file.isDirectory() && file.canRead()) {
            return true;
        } else return file.getName().toLowerCase(Locale.getDefault()).endsWith("pdf");

    }
}