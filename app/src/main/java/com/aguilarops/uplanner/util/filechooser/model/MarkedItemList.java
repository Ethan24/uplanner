package com.aguilarops.uplanner.util.filechooser.model;

public class MarkedItemList {

    private static String selectedItem = null;

    public static boolean hasItem(String key) {
        return (selectedItem != null) && selectedItem.equals(key);
    }

    public static void clearSelectionList() {
        selectedItem = null;
    }

    public static void addSingleFile(FileListItem item) {
        selectedItem = item.getLocation();
    }

    public static String getSelectedItem() {
        return selectedItem;
    }

}
