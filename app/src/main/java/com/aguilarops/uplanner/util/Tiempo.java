package com.aguilarops.uplanner.util;

public class Tiempo {
    public int horas;
    public int minutos;

    public Tiempo(int horas, int minutos) {
        this.horas = horas;
        this.minutos = minutos;
    }

    public Tiempo(String cadena) {
        setValue(cadena);
    }

    public static Tiempo getValue(String cadena) {
        boolean isPM = false;
        if (cadena.toLowerCase().contains("pm")) isPM = true;
        cadena = cadena.toLowerCase().replace("am", "").replace("pm", "");
        String[] parts_of_hora = cadena.trim().split(":");
        int hora_first = Integer.parseInt(parts_of_hora[0]);

        if (isPM && hora_first < 12) hora_first += 12;

        return new Tiempo(hora_first, Integer.parseInt(parts_of_hora[1]));
    }

    private void setValue(String cadena) {
        boolean isPM = false;
        if (cadena.toLowerCase().contains("pm")) isPM = true;
        cadena = cadena.toLowerCase().replace("am", "").replace("pm", "");
        String[] parts_of_hora = cadena.trim().split(":");
        int hora_first = Integer.parseInt(parts_of_hora[0]);

        if (isPM && hora_first < 12) hora_first += 12;

        this.horas = hora_first;
        this.minutos = Integer.parseInt(parts_of_hora[1]);
    }
}
