package com.aguilarops.uplanner.util.filechooser.controller;

/**
 * @author akshay sunil masram
 */
public interface DialogSelectionListener {
    void onSelectedFilePaths(String files);
}