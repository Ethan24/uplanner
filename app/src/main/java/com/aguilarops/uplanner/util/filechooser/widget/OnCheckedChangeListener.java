package com.aguilarops.uplanner.util.filechooser.widget;

public interface OnCheckedChangeListener {
    void onCheckedChanged(MaterialCheckbox checkbox, boolean isChecked);
}
