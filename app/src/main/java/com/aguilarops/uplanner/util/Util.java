package com.aguilarops.uplanner.util;

import android.annotation.SuppressLint;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {

    public static String getHoraFormat_from24(int hour, int minute) {
        Calendar cal1 = Calendar.getInstance();
        cal1.set(Calendar.HOUR_OF_DAY, hour);
        cal1.set(Calendar.MINUTE, minute);
        @SuppressLint("SimpleDateFormat") DateFormat outputformat = new SimpleDateFormat("hh:mm aa");
        return outputformat.format(cal1.getTime());
    }

    public static int getNowTimeWeight() {
        Calendar cal = Calendar.getInstance();
        return getWeight_hour(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));
    }

    public static int getcount(
            String someString, char searchedChar) {
        if (someString.isEmpty()) return 0;

        int count = someString.charAt(0) == searchedChar ? 1 : 0;
        return count + countOccurences(
                someString, searchedChar, 1);
    }

    public static int getWeight_hour(int hour, int minute) {
        return Integer.parseInt(String.valueOf(hour) + minute);
    }

    private static int countOccurences(
            String someString, char searchedChar, int index) {
        if (index >= someString.length()) {
            return 0;
        }

        int count = someString.charAt(index) == searchedChar ? 1 : 0;
        return count + countOccurences(
                someString, searchedChar, index + 1);
    }

    public static List<Integer> extraerNumeros(String cadena) {
        List<Integer> todosLosNumeros = new ArrayList<>();
        Matcher encuentrador = Pattern.compile("\\d+").matcher(cadena);
        while (encuentrador.find()) {
            todosLosNumeros.add(Integer.parseInt(encuentrador.group()));
        }
        return todosLosNumeros;
    }

    public static int getDaysFromnow2Date(String date) {
        System.out.println("Fecha string " + date);
        Date date1 = getDate_fromFormat(date);
        Date date2 = new Date();
        date2.setTime(System.currentTimeMillis());
        float diff = date1.getTime() - date2.getTime();
        float segundos = diff / 1000;
        float minutos = segundos / 60;
        float horas = minutos / 60;
        int entero = (int) (horas / 24);
        float temp = horas / 24;
        temp = temp - entero > 0 ? entero + 1 : temp;
        long dias = (int) temp;
        return (int) dias;
    }

    public static Date getDate_fromFormat(String formated) {
        Date date = null;
        if (Locale.getDefault() != Locale.US) {
            StringBuilder strb = new StringBuilder(formated);
            strb.setCharAt(0, Character.toLowerCase(strb.charAt(0)));
            strb.setCharAt(5, Character.toLowerCase(strb.charAt(5)));

            formated = strb.toString();
            System.out.println(strb.toString());
        }
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("EEE, MMM d, yyyy");

        try {
            date = formatter.parse(formated);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(date);
        return date;
    }

    public static String dateFormat(Date time) {
        String formated = null;
        DateFormat dfEn = new SimpleDateFormat(
                "EEE, MMM d, yyyy", Locale.getDefault());
        try {
            StringBuilder strb = new StringBuilder(dfEn.format(time));
            strb.setCharAt(0, Character.toUpperCase(strb.charAt(0)));
            strb.setCharAt(5, Character.toUpperCase(strb.charAt(5)));
            formated = strb.toString();
        } catch (Exception ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        }
        return formated;
    }

    public static int getYear() {
        Calendar fecha = Calendar.getInstance();
        return fecha.get(Calendar.YEAR);
    }

    public static boolean isNumeric(String cadena) {
        if (cadena == null || cadena.isEmpty()) {
            return false;
        }

        int index = 0;

        while (index < cadena.length() && Character.isDigit(cadena.charAt(index))) {
            index++;
        }

        return index == cadena.length();
    }

    public static boolean isValid_email(String email) {
        String regex = "^[\\w-_.+]*[\\w-_.]@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    public static boolean isValid_phone(String phone) {
        return phone.matches(Phone.regex_international) || phone.matches(Phone.regex_nica);
    }

    public static int getPhoneType(String phone) {
        return phone.matches(Phone.regex_international) ? 0 : 1;
    }

    static class Phone {
        static String regex_international = "^(\\+\\d{1,3}( )?)?((\\(\\d{3}\\))|\\d{3})[- .]?\\d{3}[- .]?\\d{4}$";


        static String regex_nica = "^(\\+\\d{3}[- ]?)?(\\d{4}[- ]?)\\d{4}$";
    }
}
